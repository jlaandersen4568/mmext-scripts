local i4, i2, i1, u4, u2, u1, call = mem.i4, mem.i2, mem.i1, mem.u4, mem.u2, mem.u1, mem.call

mem.IgnoreProtection(true)

-- allow choosing dragons on start
u1[0x43385B+2] = 23 + 2
u1[0x433928+2] = 23 + 2
-- allow choosing dragon voices on start
u1[0x4337F3-4] = 23 + 2
u1[0x433794-1] = 23 + 2

-- don't draw body and hands for dragon
mem.hook(0x4C4F9A, function(d)
	d.ecx = i4[d.edi + 0x1A8]
	if i4[d.ebp - 8] == 4 then
		u4[d.esp] = 0x4C50CB
	end
end, 6)

mem.IgnoreProtection(false)

local setup = {[0] = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 2, 0, 0, 1, 1}

local function UpdateStartingSkills()
	table.copy(setup, Game.ClassKinds.StartingSkills[const.Class.Dragon/2], true)
end

UpdateStartingSkills()

function events.GameInitialized2()
	UpdateStartingSkills()
end
