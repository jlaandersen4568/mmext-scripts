str[0] = " "
str[1] = "Magic Door"
str[2] = "Bag"
str[3] = "You cannot see me, hear me or touch me.  I lie behind the stars and alter what is real, I am what you really fear, Close your eyes and come I near. What am I?"
str[4] = "dark"
str[5] = "darkness"
str[6] = "Answer?  "
str[7] = "Incorrect"
str[8] = "I go through an apple, or point out your way, I fit in a bow, then a target, to stay. What am I?"
str[9] = "arrow"
str[10] = "an arrow"
str[11] = "What consumes rocks, levels mountains, destroys wood, pushes the clouds across the sky, and can make a young one old?"
str[12] = "time"
str[13] = "Alive without breath, as cold as death, never thirsty ever drinking, all in mail never clinking, ever travelling, never walking, mouth ever moving, never talking.  What am I? "
str[14] = "fish"
str[15] = "a fish"
str[16] = "?"
str[17] = "Sack"
str[18] = "Empty"
str[19] = ""


event 1
      Hint = str[1]  -- "Magic Door"
  0:  SetMessage  {Str = 67587}
  1:  Question  {Question = 67437574, Answer1 = 33554693, Answer2 = 17106717,   jump(ok) = 0}
  2:  StatusText  {Str = 66823}
  3:  Exit  {}

  4:  SetDoorState  {Id = 1, State = 1}
end

event 2
      Hint = str[1]  -- "Magic Door"
  0:  SetMessage  {Str = 133128}
  1:  Question  {Question = 67766534, Answer1 = 33554949, Answer2 = 33883933,   jump(ok) = 0}
  2:  StatusText  {Str = 132359}
  3:  Exit  {}

  4:  SetDoorState  {Id = 2, State = 1}
end

event 3
      Hint = str[1]  -- "Magic Door"
  0:  SetMessage  {Str = 198667}
  1:  Question  {Question = 67898374, Answer1 = 33555205, Answer2 = 50661149,   jump(ok) = 0}
  2:  StatusText  {Str = 197895}
  3:  Exit  {}

  4:  SetDoorState  {Id = 3, State = 1}
end

event 4
      Hint = str[1]  -- "Magic Door"
  0:  SetMessage  {Str = 264205}
  1:  Question  {Question = 68095494, Answer1 = 33555461, Answer2 = 67438365,   jump(ok) = 0}
  2:  StatusText  {Str = 263431}
  3:  Exit  {}

  4:  SetDoorState  {Id = 4, State = 1}
end

event 5
  0:  MoveToMap  {X = 0, Y = 0, Z = 0, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "d03.blv"}
  1:  Subtract  {"Food", Value = 5}
end

event 6
  0:  MoveToMap  {X = 0, Y = 0, Z = 0, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "d08.blv"}
  1:  Subtract  {"Food", Value = 5}
end

event 7
  0:  MoveToMap  {X = 0, Y = 0, Z = 0, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "d06.blv"}
  1:  Subtract  {"Food", Value = 5}
end

event 8
  0:  MoveToMap  {X = 11407, Y = 18074, Z = 1, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "outc2.odm"}
  1:  Subtract  {"Food", Value = 5}
end

event 9
      Hint = str[16]  -- "?"
  0:  Exit  {}
end

event 10
      Hint = str[17]  -- "Sack"
  0:  Cmp  {"MapVar0", Value = 1,   jump = 4}
  1:  Set  {"MapVar0", Value = 1}
  2:  Add  {"FoodAddRandom", Value = 4}
  3:  Exit  {}

  4:  StatusText  {Str = 722194}
end

event 11
      Hint = str[17]  -- "Sack"
  0:  Cmp  {"MapVar1", Value = 1,   jump = 4}
  1:  Set  {"MapVar1", Value = 1}
  2:  Add  {"FoodAddRandom", Value = 4}
  3:  Exit  {}

  4:  StatusText  {Str = 787730}
end

event 12
      Hint = str[17]  -- "Sack"
  0:  Cmp  {"MapVar2", Value = 1,   jump = 4}
  1:  Set  {"MapVar2", Value = 1}
  2:  Add  {"FoodAddRandom", Value = 4}
  3:  Exit  {}

  4:  StatusText  {Str = 853266}
end

event 13
      Hint = str[17]  -- "Sack"
  0:  Cmp  {"MapVar3", Value = 1,   jump = 3}
  1:  Set  {"MapVar3", Value = 1}
  2:  Add  {"FoodAddRandom", Value = 3}
  3:  Exit  {}

  4:  StatusText  {Str = 918802}
end

event 14
      Hint = str[2]  -- "Bag"
  0:  OpenChest  {Id = 1}
end

event 15
      Hint = str[2]  -- "Bag"
  0:  OpenChest  {Id = 2}
end

event 16
      Hint = str[2]  -- "Bag"
  0:  OpenChest  {Id = 3}
end

event 17
      Hint = str[2]  -- "Bag"
  0:  OpenChest  {Id = 4}
end

event 18
      Hint = str[2]  -- "Bag"
  0:  OpenChest  {Id = 5}
end

event 19
      Hint = str[2]  -- "Bag"
  0:  OpenChest  {Id = 6}
end

event 20
      Hint = str[2]  -- "Bag"
  0:  StatusText  {Str = 1377554}
end

event 21
  0:  OnLoadMap  {}
  1:  SetDoorState  {Id = 1, State = 0}
  2:  SetDoorState  {Id = 2, State = 0}
  3:  SetDoorState  {Id = 3, State = 0}
  4:  SetDoorState  {Id = 4, State = 0}
end
