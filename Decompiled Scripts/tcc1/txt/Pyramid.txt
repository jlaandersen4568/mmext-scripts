str[0] = " "
str[1] = "Warning!  Sensor array controls are strictly off limits to unauthorized personnel!  Use of the Sign of Sight is restricted to communications technicians only!  A mild electric shock will be transmitted to violators."
str[2] = "Warning!  Cargo lift controls are strictly off limits to unauthorized personnel!  Use of the Sign of the Scarab is restricted to supply officers only!  A mild electric shock will be transmitted to violators."
str[3] = "Trap!"
str[4] = "Radiation Damage!"
str[5] = "Plaque"
str[6] = "The entrance to the central pyramid lies to the South."
str[7] = "The door is locked"
str[8] = "CleansingPool"
str[9] = "Flame Door"
str[10] = "Radiation Damage!"
str[11] = "The chest is locked"
str[12] = "Door won't budge."
str[13] = "Radiation Damage!"
str[14] = "Radiation Damage!"
str[15] = "The waters part."
str[16] = "Warning!  Power Fluctuations!  Alert Engineering immediately!"
str[17] = "In case of energy leak, bathe in one of the medicated pools placed for your safety and convenience."
str[18] = "Crystal Skull absorbs radiation damage."
str[19] = "Door"
str[20] = "Only the one bearing the key may speak the code."
str[21] = "Well of VARN"
str[22] = "Picture Door"
str[23] = "Back Door"
str[24] = "Switch"
str[25] = "Water Temple"
str[26] = "Cleansing Pool"
str[27] = "The Well of VARN must be keyed last."
str[28] = "Control Room Entry"
str[29] = "Chest"
str[30] = "Picture"
str[31] = "Exit"
str[32] = "What is the first mate's code?"
str[33] = "kcopS"
str[34] = "What is the navigator's code?"
str[35] = "uluS"
str[36] = "What is the communication officer's code?"
str[37] = "aruhU"
str[38] = "What is the engineer's code?"
str[39] = "yttocS"
str[40] = "What is the doctor's code?"
str[41] = "yoccM"
str[42] = "What is the Captain's code?"
str[43] = "kriK"
str[44] = "Answer?  "
str[45] = "Incorrect."
str[46] = "Access Denied.  All codes must be entered first."
str[47] = "Main Power failed.  Emergency power on."
str[48] = "Main Power restored."
str[49] = "Books"
str[50] = "Tomb of Varn"
str[51] = "Bookcase"
str[52] = "Tapestry"
str[53] = "With painstaking care, you are able to decipher the message of the hieroglyphs:                                                                                                                                                     Though the Crossing of the Void be a long and arduous journey, the land you find at the end will be sweet and unspoiled by ancestors or the Enemy.  Take heart that your children's children will live in a perfect world free of war, free of famine, and free of fear.  Remember your sacred duty to care for the Ship on her long Voyage and ensure her safe arrival in the Promised Land.  Tend well the Guardian and house it securely away from the ship lest both be lost in a single misfortune."
str[54] = "With painstaking care, you are able to decipher the message of the hieroglyphs, intermixed with diagrams of devils:                                                                                          Remember our Enemy, children, and never underestimate the danger they pose.  Though you will never see one during your journey, you must be forever vigilant against invasion from the Void once the Voyage has ended.  Mighty beyond words, the Enemy is nonetheless vulnerable after a Crossing, for their numbers are small and their defenses weak.  Use the energy weapons carried on the Ship to defeat them, and never, ever engage the Enemy with lesser weapons, or you will surely perish."
str[55] = ""


event 1
      Hint = str[21]  -- "Well of VARN"
      MazeInfo = str[50]  -- "Tomb of Varn"
  0:  Set  {"MapVar0", Value = 0}
  1:  Cmp  {"Inventory", Value = 537,   jump = 3}         -- "Forbeo�s Winter Axe"
  2:  Exit  {}

  3:  Cmp  {"MapVar27", Value = 1,   jump = 8}
  4:  StatusText  {Str = 46}         -- "Access Denied.  All codes must be entered first."
  5:  Subtract  {"HP", Value = 25}
  6:  FaceExpression  {Player = "Current", Frame = 35}
  7:  Exit  {}

  8:  SetMessage  {Str = 42}         -- "What is the Captain's code?"
  9:  Question  {Question = 44, Answer1 = 43, Answer2 = 43,   jump(ok) = 14}         -- "Answer?  " ("kriK")
  10: StatusText  {Str = 45}         -- "Incorrect."
  11: FaceExpression  {Player = "Current", Frame = 44}
  12: Subtract  {"HP", Value = 5}
  13: Exit  {}

  14: Set  {"MapVar15", Value = 1}
  15: Subtract  {"Inventory", Value = 537}         -- "Forbeo�s Winter Axe"
  16: Subtract  {"QBits", Value = 231}         -- Quest item bits for seer
  17: SetDoorState  {Id = 1, State = 1}
  18: StatusText  {Str = 15}         -- "The waters part."
end

event 2
      Hint = str[22]  -- "Picture Door"
  0:  SetDoorState  {Id = 2, State = 1}
end

event 3
      Hint = str[23]  -- "Back Door"
  0:  Cmp  {"Inventory", Value = 575,   jump = 3}         -- "Back Door Key"
  1:  StatusText  {Str = 7}         -- "The door is locked"
  2:  Exit  {}

  3:  Cmp  {"CurrentMight", Value = 35,   jump = 6}
  4:  StatusText  {Str = 12}         -- "Door won't budge."
  5:  Exit  {}

  6:  Subtract  {"Inventory", Value = 575}         -- "Back Door Key"
  7:  SetDoorState  {Id = 3, State = 1}
end

event 4
      Hint = str[22]  -- "Picture Door"
  0:  SetDoorState  {Id = 4, State = 1}
end

event 5
      Hint = str[19]  -- "Door"
  0:  SetDoorState  {Id = 5, State = 1}
end

event 6
      Hint = str[22]  -- "Picture Door"
  0:  SetDoorState  {Id = 6, State = 1}
end

event 7
      Hint = str[22]  -- "Picture Door"
  0:  SetDoorState  {Id = 7, State = 1}
end

event 8
      Hint = str[24]  -- "Switch"
  0:  SetDoorState  {Id = 8, State = 2}         -- switch state
  1:  StatusText  {Str = 3}         -- "Trap!"
  2:  DamagePlayer  {Player = "Current", DamageType = const.Damage.Fire, Damage = 200}
end

event 9
      Hint = str[24]  -- "Switch"
  0:  SetDoorState  {Id = 9, State = 2}         -- switch state
  1:  StatusText  {Str = 3}         -- "Trap!"
  2:  DamagePlayer  {Player = "Current", DamageType = const.Damage.Magic, Damage = 200}
end

event 10
      Hint = str[22]  -- "Picture Door"
  0:  SetDoorState  {Id = 10, State = 1}
end

event 11
      Hint = str[25]  -- "Water Temple"
  0:  Cmp  {"Inventory", Value = 573,   jump = 3}         -- "Ullr�s Bow"
  1:  StatusText  {Str = 7}         -- "The door is locked"
  2:  Exit  {}

  3:  Subtract  {"Inventory", Value = 573}         -- "Ullr�s Bow"
  4:  SetDoorState  {Id = 11, State = 1}
end

event 12
  0:  OnTimer  {IntervalInHalfMinutes = 3}
  1:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  2:  Exit  {}

  3:  ForPlayer  ("Random")
  4:  Cmp  {"Inventory", Value = 466,   jump = 24}         -- "Crystal Skull"
  5:  RandomGoTo  {jumpA = 6, jumpB = 9, jumpC = 12, jumpD = 15, jumpE = 18, jumpF = 21}
  6:  DamagePlayer  {Player = "All", DamageType = const.Damage.Cold, Damage = 5}
  7:  StatusText  {Str = 4}         -- "Radiation Damage!"
  8:  Exit  {}

  9:  DamagePlayer  {Player = "All", DamageType = const.Damage.Fire, Damage = 5}
  10: StatusText  {Str = 10}         -- "Radiation Damage!"
  11: Exit  {}

  12: DamagePlayer  {Player = "All", DamageType = const.Damage.Elec, Damage = 5}
  13: StatusText  {Str = 13}         -- "Radiation Damage!"
  14: Exit  {}

  15: DamagePlayer  {Player = "All", DamageType = const.Damage.Magic, Damage = 5}
  16: StatusText  {Str = 14}         -- "Radiation Damage!"
  17: Exit  {}

  18: DamagePlayer  {Player = "All", DamageType = const.Damage.Phys, Damage = 5}
  19: StatusText  {Str = 13}         -- "Radiation Damage!"
  20: Exit  {}

  21: DamagePlayer  {Player = "All", DamageType = const.Damage.Poison, Damage = 5}
  22: StatusText  {Str = 10}         -- "Radiation Damage!"
  23: Exit  {}

  24: StatusText  {Str = 18}         -- "Crystal Skull absorbs radiation damage."
  25: Exit  {}
end

event 13
      Hint = str[26]  -- "Cleansing Pool"
  0:  Set  {"MapVar0", Value = 0}
end

event 14
  0:  OnTimer  {IntervalInHalfMinutes = 4}
  1:  Cmp  {"MapVar2", Value = 1,   jump = 18}
  2:  Set  {"MapVar2", Value = 1}
  3:  Cmp  {"MapVar3", Value = 1,   jump = 5}
  4:  GoTo  {jump = 6}

  5:  StatusText  {Str = 47}         -- "Main Power failed.  Emergency power on."
  6:  SetLight  {Id = 35, On = false}
  7:  SetLight  {Id = 43, On = false}
  8:  SetLight  {Id = 34, On = false}
  9:  SetLight  {Id = 36, On = false}
  10: SetLight  {Id = 37, On = false}
  11: SetLight  {Id = 33, On = false}
  12: SetLight  {Id = 42, On = true}
  13: SetLight  {Id = 41, On = true}
  14: SetLight  {Id = 38, On = true}
  15: SetLight  {Id = 40, On = true}
  16: SetLight  {Id = 39, On = true}
  17: Exit  {}

  18: Set  {"MapVar2", Value = 0}
  19: Cmp  {"MapVar3", Value = 1,   jump = 21}
  20: GoTo  {jump = 23}

  21: Add  {"MapVar3", Value = 1}
  22: StatusText  {Str = 48}         -- "Main Power restored."
  23: SetLight  {Id = 35, On = true}
  24: SetLight  {Id = 43, On = true}
  25: SetLight  {Id = 34, On = true}
  26: SetLight  {Id = 36, On = true}
  27: SetLight  {Id = 37, On = true}
  28: SetLight  {Id = 33, On = true}
  29: SetLight  {Id = 42, On = false}
  30: SetLight  {Id = 41, On = false}
  31: SetLight  {Id = 38, On = false}
  32: SetLight  {Id = 40, On = false}
  33: SetLight  {Id = 39, On = false}
  34: Cmp  {"MapVar3", Value = 3,   jump = 36}
  35: Exit  {}

  36: Set  {"MapVar3", Value = 0}
end

event 15
  0:  SetDoorState  {Id = 15, State = 1}
end

event 16
      Hint = str[22]  -- "Picture Door"
  0:  Cmp  {"CurrentMight", Value = 25,   jump = 3}
  1:  StatusText  {Str = 12}         -- "Door won't budge."
  2:  Exit  {}

  3:  SetDoorState  {Id = 16, State = 1}
end

event 17
      Hint = str[31]  -- "Exit"
  0:  MoveToMap  {X = -6647, Y = 13018, Z = 1761, Direction = 1536, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 6, Name = "OutB3.Odm"}
end

event 18
      Hint = str[31]  -- "Exit"
  0:  MoveToMap  {X = -6611, Y = 11408, Z = 480, Direction = 1536, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 6, Name = "OutB3.Odm"}
end

event 19
      Hint = str[30]  -- "Picture"
  0:  Exit  {}
end

event 20
  0:  SetDoorState  {Id = 16, State = 0}
end

event 21
      Hint = str[26]  -- "Cleansing Pool"
  0:  Set  {"MapVar0", Value = 0}
  1:  Cmp  {"Inventory", Value = 538,   jump = 3}         -- "Staff of Vallah"
  2:  Exit  {}

  3:  SetMessage  {Str = 32}         -- "What is the first mate's code?"
  4:  Question  {Question = 44, Answer1 = 33, Answer2 = 33,   jump(ok) = 9}         -- "Answer?  " ("kcopS")
  5:  StatusText  {Str = 45}         -- "Incorrect."
  6:  FaceExpression  {Player = "Current", Frame = 48}
  7:  Subtract  {"HP", Value = 5}
  8:  Exit  {}

  9:  Cmp  {"MapVar11", Value = 1,   jump = 11}
  10: GoTo  {jump = 18}

  11: Cmp  {"MapVar12", Value = 1,   jump = 13}
  12: GoTo  {jump = 18}

  13: Cmp  {"MapVar13", Value = 1,   jump = 15}
  14: GoTo  {jump = 18}

  15: Cmp  {"MapVar14", Value = 1,   jump = 17}
  16: GoTo  {jump = 18}

  17: Set  {"MapVar27", Value = 1}
  18: Set  {"MapVar10", Value = 1}
  19: ForPlayer  ("All")
  20: Subtract  {"Inventory", Value = 538}         -- "Staff of Vallah"
  21: Subtract  {"QBits", Value = 229}         -- Quest item bits for seer
end

event 22
      Hint = str[26]  -- "Cleansing Pool"
  0:  Set  {"MapVar0", Value = 0}
  1:  Cmp  {"Inventory", Value = 539,   jump = 3}         -- "Skadi's Justice Crossbow"
  2:  Exit  {}

  3:  SetMessage  {Str = 34}         -- "What is the navigator's code?"
  4:  Question  {Question = 44, Answer1 = 35, Answer2 = 35,   jump(ok) = 9}         -- "Answer?  " ("uluS")
  5:  StatusText  {Str = 45}         -- "Incorrect."
  6:  FaceExpression  {Player = "Current", Frame = 33}
  7:  Subtract  {"HP", Value = 5}
  8:  Exit  {}

  9:  Cmp  {"MapVar10", Value = 1,   jump = 11}
  10: GoTo  {jump = 18}

  11: Cmp  {"MapVar12", Value = 1,   jump = 13}
  12: GoTo  {jump = 18}

  13: Cmp  {"MapVar13", Value = 1,   jump = 15}
  14: GoTo  {jump = 18}

  15: Cmp  {"MapVar14", Value = 1,   jump = 17}
  16: GoTo  {jump = 18}

  17: Set  {"MapVar27", Value = 1}
  18: Set  {"MapVar11", Value = 1}
  19: ForPlayer  ("All")
  20: Subtract  {"Inventory", Value = 539}         -- "Skadi's Justice Crossbow"
  21: Subtract  {"QBits", Value = 232}         -- Quest item bits for seer
end

event 23
      Hint = str[26]  -- "Cleansing Pool"
  0:  Set  {"MapVar0", Value = 0}
  1:  Cmp  {"Inventory", Value = 540,   jump = 3}         -- "Freya�s Vengeance Spear"
  2:  Exit  {}

  3:  SetMessage  {Str = 36}         -- "What is the communication officer's code?"
  4:  Question  {Question = 44, Answer1 = 37, Answer2 = 37,   jump(ok) = 9}         -- "Answer?  " ("aruhU")
  5:  StatusText  {Str = 45}         -- "Incorrect."
  6:  FaceExpression  {Player = "Current", Frame = 50}
  7:  Subtract  {"HP", Value = 5}
  8:  Exit  {}

  9:  Cmp  {"MapVar11", Value = 1,   jump = 11}
  10: GoTo  {jump = 18}

  11: Cmp  {"MapVar10", Value = 1,   jump = 13}
  12: GoTo  {jump = 18}

  13: Cmp  {"MapVar13", Value = 1,   jump = 15}
  14: GoTo  {jump = 18}

  15: Cmp  {"MapVar14", Value = 1,   jump = 17}
  16: GoTo  {jump = 18}

  17: Set  {"MapVar27", Value = 1}
  18: Set  {"MapVar12", Value = 1}
  19: ForPlayer  ("All")
  20: Subtract  {"Inventory", Value = 540}         -- "Freya�s Vengeance Spear"
  21: Subtract  {"QBits", Value = 234}         -- Quest item bits for seer
end

event 24
      Hint = str[26]  -- "Cleansing Pool"
  0:  Set  {"MapVar0", Value = 0}
  1:  Cmp  {"Inventory", Value = 541,   jump = 3}         -- "The Story of Danu's Treasure"
  2:  Exit  {}

  3:  SetMessage  {Str = 38}         -- "What is the engineer's code?"
  4:  Question  {Question = 44, Answer1 = 39, Answer2 = 39,   jump(ok) = 9}         -- "Answer?  " ("yttocS")
  5:  StatusText  {Str = 45}         -- "Incorrect."
  6:  FaceExpression  {Player = "Current", Frame = 46}
  7:  Subtract  {"HP", Value = 5}
  8:  Exit  {}

  9:  Cmp  {"MapVar11", Value = 1,   jump = 11}
  10: GoTo  {jump = 18}

  11: Cmp  {"MapVar12", Value = 1,   jump = 13}
  12: GoTo  {jump = 18}

  13: Cmp  {"MapVar10", Value = 1,   jump = 15}
  14: GoTo  {jump = 18}

  15: Cmp  {"MapVar14", Value = 1,   jump = 17}
  16: GoTo  {jump = 18}

  17: Set  {"MapVar27", Value = 1}
  18: Set  {"MapVar13", Value = 1}
  19: ForPlayer  ("All")
  20: Subtract  {"Inventory", Value = 541}         -- "The Story of Danu's Treasure"
  21: Subtract  {"QBits", Value = 233}         -- Quest item bits for seer
end

event 25
      Hint = str[26]  -- "Cleansing Pool"
  0:  Set  {"MapVar0", Value = 0}
  1:  Cmp  {"Inventory", Value = 542,   jump = 3}         -- "Doctor's Code"
  2:  Exit  {}

  3:  SetMessage  {Str = 40}         -- "What is the doctor's code?"
  4:  Question  {Question = 44, Answer1 = 41, Answer2 = 41,   jump(ok) = 9}         -- "Answer?  " ("yoccM")
  5:  StatusText  {Str = 45}         -- "Incorrect."
  6:  FaceExpression  {Player = "Current", Frame = 13}
  7:  Subtract  {"HP", Value = 5}
  8:  Exit  {}

  9:  Cmp  {"MapVar11", Value = 1,   jump = 11}
  10: GoTo  {jump = 18}

  11: Cmp  {"MapVar12", Value = 1,   jump = 13}
  12: GoTo  {jump = 18}

  13: Cmp  {"MapVar13", Value = 1,   jump = 15}
  14: GoTo  {jump = 18}

  15: Cmp  {"MapVar10", Value = 1,   jump = 17}
  16: GoTo  {jump = 18}

  17: Set  {"MapVar27", Value = 1}
  18: Set  {"MapVar14", Value = 1}
  19: ForPlayer  ("All")
  20: Subtract  {"Inventory", Value = 542}         -- "Doctor's Code"
  21: Subtract  {"QBits", Value = 230}         -- Quest item bits for seer
end

event 26
      Hint = str[49]  -- "Books"
  0:  Cmp  {"MapVar25", Value = 1,   jump = 4}
  1:  Set  {"QBits", Value = 234}         -- Quest item bits for seer
  2:  Add  {"Inventory", Value = 540}         -- "Freya�s Vengeance Spear"
  3:  Set  {"MapVar25", Value = 1}
  4:  Exit  {}
end

event 27
      Hint = str[49]  -- "Books"
  0:  Cmp  {"MapVar26", Value = 1,   jump = 4}
  1:  Set  {"QBits", Value = 233}         -- Quest item bits for seer
  2:  Add  {"Inventory", Value = 541}         -- "The Story of Danu's Treasure"
  3:  Set  {"MapVar26", Value = 1}
  4:  Exit  {}
end

event 28
  0:  Set  {"MapVar3", Value = 1}
end

event 29
      Hint = str[24]  -- "Switch"
  0:  SetDoorState  {Id = 38, State = 1}
  1:  SetDoorState  {Id = 37, State = 1}
end

event 30
      Hint = str[22]  -- "Picture Door"
  0:  Cmp  {"CurrentMight", Value = 20,   jump = 3}
  1:  StatusText  {Str = 12}         -- "Door won't budge."
  2:  Exit  {}

  3:  SetDoorState  {Id = 35, State = 1}
  4:  SetDoorState  {Id = 36, State = 1}
end

event 31
      Hint = str[9]  -- "Flame Door"
  0:  Cmp  {"Inventory", Value = 574,   jump = 3}         -- "Common Chest Key"
  1:  StatusText  {Str = 7}         -- "The door is locked"
  2:  Exit  {}

  3:  Cmp  {"CurrentMight", Value = 25,   jump = 6}
  4:  StatusText  {Str = 12}         -- "Door won't budge."
  5:  Exit  {}

  6:  Subtract  {"Inventory", Value = 574}         -- "Common Chest Key"
  7:  SetDoorState  {Id = 32, State = 1}
  8:  SetDoorState  {Id = 31, State = 1}
end

event 32
      Hint = str[24]  -- "Switch"
  0:  SetDoorState  {Id = 33, State = 2}         -- switch state
  1:  SetDoorState  {Id = 34, State = 2}         -- switch state
end

event 33
  0:  MoveToMap  {X = -9344, Y = -192, Z = 8034, Direction = 1, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  1:  SetDoorState  {Id = 33, State = 0}
end

event 35
      Hint = str[28]  -- "Control Room Entry"
  0:  Cmp  {"MapVar15", Value = 1,   jump = 2}
  1:  Exit  {}

  2:  SetDoorState  {Id = 39, State = 1}
  3:  SetDoorState  {Id = 40, State = 1}
end

event 41
      Hint = str[29]  -- "Chest"
  0:  Cmp  {"MapVar28", Value = 1,   jump = 7}
  1:  Cmp  {"QBits", Value = 42,   jump = 11}         -- Bruce
  2:  Cmp  {"Inventory", Value = 577,   jump = 5}         -- "Key to Luftka's Tomb"
  3:  StatusText  {Str = 11}         -- "The chest is locked"
  4:  Exit  {}

  5:  Subtract  {"Inventory", Value = 577}         -- "Key to Luftka's Tomb"
  6:  Set  {"MapVar28", Value = 1}
  7:  OpenChest  {Id = 1}
  8:  Set  {"QBits", Value = 42}         -- Bruce
  9:  Set  {"QBits", Value = 195}         -- Quest item bits for seer
  10: Exit  {}

  11: OpenChest  {Id = 6}
end

event 42
      Hint = str[29]  -- "Chest"
  0:  OpenChest  {Id = 2}
  1:  Set  {"QBits", Value = 229}         -- Quest item bits for seer
end

event 43
      Hint = str[29]  -- "Chest"
  0:  Cmp  {"MapVar49", Value = 1,   jump = 6}
  1:  Cmp  {"Inventory", Value = 576,   jump = 4}         -- "Forseti's Sword"
  2:  StatusText  {Str = 11}         -- "The chest is locked"
  3:  Exit  {}

  4:  Subtract  {"Inventory", Value = 576}         -- "Forseti's Sword"
  5:  Set  {"MapVar49", Value = 1}
  6:  OpenChest  {Id = 3}
  7:  Set  {"QBits", Value = 230}         -- Quest item bits for seer
end

event 44
      Hint = str[29]  -- "Chest"
  0:  OpenChest  {Id = 4}
  1:  Set  {"QBits", Value = 231}         -- Quest item bits for seer
end

event 45
      Hint = str[29]  -- "Chest"
  0:  OpenChest  {Id = 5}
  1:  Set  {"QBits", Value = 232}         -- Quest item bits for seer
end

event 46
      Hint = str[5]  -- "Plaque"
  0:  SetMessage  {Str = 1}         -- "Warning!  Sensor array controls are strictly off limits to unauthorized personnel!  Use of the Sign of Sight is restricted to communications technicians only!  A mild electric shock will be transmitted to violators."
  1:  SimpleMessage  {}
end

event 47
      Hint = str[5]  -- "Plaque"
  0:  SetMessage  {Str = 2}         -- "Warning!  Cargo lift controls are strictly off limits to unauthorized personnel!  Use of the Sign of the Scarab is restricted to supply officers only!  A mild electric shock will be transmitted to violators."
  1:  SimpleMessage  {}
end

event 48
      Hint = str[5]  -- "Plaque"
  0:  SetMessage  {Str = 6}         -- "The entrance to the central pyramid lies to the South."
  1:  SimpleMessage  {}
end

event 49
      Hint = str[29]  -- "Chest"
  0:  OpenChest  {Id = 6}
end

event 51
  0:  Set  {"MapVar0", Value = 1}
end

event 52
      Hint = str[5]  -- "Plaque"
  0:  SetMessage  {Str = 20}         -- "Only the one bearing the key may speak the code."
  1:  SimpleMessage  {}
end

event 53
      Hint = str[5]  -- "Plaque"
  0:  SetMessage  {Str = 27}         -- "The Well of VARN must be keyed last."
  1:  SimpleMessage  {}
end

event 54
      Hint = str[5]  -- "Plaque"
  0:  SetMessage  {Str = 16}         -- "Warning!  Power Fluctuations!  Alert Engineering immediately!"
  1:  SimpleMessage  {}
end

event 55
      Hint = str[5]  -- "Plaque"
  0:  SetMessage  {Str = 17}         -- "In case of energy leak, bathe in one of the medicated pools placed for your safety and convenience."
  1:  SimpleMessage  {}
end

event 56
  0:  OnRefillTimer  {EachWeek = true}
  1:  Set  {"MapVar0", Value = 0}
end

event 57
  0:  Set  {"MapVar3", Value = 0}
end

event 60
      Hint = str[51]  -- "Bookcase"
  0:  Cmp  {"MapVar29", Value = 1,   jump = 3}
  1:  Set  {"MapVar29", Value = 1}
  2:  Add  {"Inventory", Value = 308}         -- "Meteor Shower"
  3:  Exit  {}
end

event 61
      Hint = str[51]  -- "Bookcase"
  0:  Cmp  {"MapVar30", Value = 1,   jump = 3}
  1:  Set  {"MapVar30", Value = 1}
  2:  Add  {"Inventory", Value = 309}         -- "Inferno"
  3:  Exit  {}
end

event 62
      Hint = str[51]  -- "Bookcase"
  0:  Cmp  {"MapVar31", Value = 1,   jump = 3}
  1:  Set  {"MapVar31", Value = 1}
  2:  Add  {"Inventory", Value = 310}         -- "Incinerate"
  3:  Exit  {}
end

event 63
      Hint = str[51]  -- "Bookcase"
  0:  Cmp  {"MapVar32", Value = 1,   jump = 3}
  1:  Set  {"MapVar32", Value = 1}
  2:  Add  {"Inventory", Value = 319}         -- "Implosion"
  3:  Exit  {}
end

event 64
      Hint = str[51]  -- "Bookcase"
  0:  Cmp  {"MapVar33", Value = 1,   jump = 3}
  1:  Set  {"MapVar33", Value = 1}
  2:  Add  {"Inventory", Value = 320}         -- "Fly"
  3:  Exit  {}
end

event 65
      Hint = str[51]  -- "Bookcase"
  0:  Cmp  {"MapVar34", Value = 1,   jump = 3}
  1:  Set  {"MapVar34", Value = 1}
  2:  Add  {"Inventory", Value = 321}         -- "Starburst"
  3:  Exit  {}
end

event 66
      Hint = str[51]  -- "Bookcase"
  0:  Cmp  {"MapVar35", Value = 1,   jump = 3}
  1:  Set  {"MapVar35", Value = 1}
  2:  Add  {"Inventory", Value = 330}         -- "Town Portal"
  3:  Exit  {}
end

event 67
      Hint = str[51]  -- "Bookcase"
  0:  Cmp  {"MapVar36", Value = 1,   jump = 3}
  1:  Set  {"MapVar36", Value = 1}
  2:  Add  {"Inventory", Value = 331}         -- "Ice Blast"
  3:  Exit  {}
end

event 68
      Hint = str[51]  -- "Bookcase"
  0:  Cmp  {"MapVar37", Value = 1,   jump = 3}
  1:  Set  {"MapVar37", Value = 1}
  2:  Add  {"Inventory", Value = 332}         -- "Lloyd's Beacon"
  3:  Exit  {}
end

event 69
      Hint = str[51]  -- "Bookcase"
  0:  Cmp  {"MapVar38", Value = 1,   jump = 3}
  1:  Set  {"MapVar38", Value = 1}
  2:  Add  {"Inventory", Value = 341}         -- "Turn to Stone"
  3:  Exit  {}
end

event 70
      Hint = str[51]  -- "Bookcase"
  0:  Cmp  {"MapVar39", Value = 1,   jump = 3}
  1:  Set  {"MapVar39", Value = 1}
  2:  Add  {"Inventory", Value = 342}         -- "Death Blossom"
  3:  Exit  {}
end

event 71
      Hint = str[51]  -- "Bookcase"
  0:  Cmp  {"MapVar40", Value = 1,   jump = 3}
  1:  Set  {"MapVar40", Value = 1}
  2:  Add  {"Inventory", Value = 343}         -- "Mass Distortion"
  3:  Exit  {}
end

event 72
      Hint = str[52]  -- "Tapestry"
  0:  SetMessage  {Str = 53}         -- "With painstaking care, you are able to decipher the message of the hieroglyphs:                                                                                                                                                     Though the Crossing of the Void be a long and arduous journey, the land you find at the end will be sweet and unspoiled by ancestors or the Enemy.  Take heart that your children's children will live in a perfect world free of war, free of famine, and free of fear.  Remember your sacred duty to care for the Ship on her long Voyage and ensure her safe arrival in the Promised Land.  Tend well the Guardian and house it securely away from the ship lest both be lost in a single misfortune."
  1:  SimpleMessage  {}
end

event 73
      Hint = str[52]  -- "Tapestry"
  0:  SetMessage  {Str = 54}         -- "With painstaking care, you are able to decipher the message of the hieroglyphs, intermixed with diagrams of devils:                                                                                          Remember our Enemy, children, and never underestimate the danger they pose.  Though you will never see one during your journey, you must be forever vigilant against invasion from the Void once the Voyage has ended.  Mighty beyond words, the Enemy is nonetheless vulnerable after a Crossing, for their numbers are small and their defenses weak.  Use the energy weapons carried on the Ship to defeat them, and never, ever engage the Enemy with lesser weapons, or you will surely perish."
  1:  SimpleMessage  {}
end
