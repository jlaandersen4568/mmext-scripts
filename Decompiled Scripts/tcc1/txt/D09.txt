str[0] = " "
str[1] = "Door"
str[2] = "Switch"
str[3] = "Double Door"
str[4] = "Chest"
str[5] = "Cabinet"
str[6] = "Bookshelves"
str[7] = "You thumb through the books, but find nothing of interest."
str[8] = "The Door is Locked"
str[9] = "Exit"
str[10] = "Pool"
str[11] = "+10 Hit points restored."
str[12] = "+10 Spell points restored."
str[13] = "Refreshing!"
str[14] = "You are not smart enough!"
str[15] = "Glastonbury Tor"
str[16] = ""


event 1
      Hint = str[1]  -- "Door"
      MazeInfo = str[15]  -- "Glastonbury Tor"
  0:  SetDoorState  {Id = 1, State = 1}
end

event 2
      Hint = str[1]  -- "Door"
  0:  Cmp  {"Inventory", Value = 572,   jump = 3}         -- "Cell Key"
  1:  StatusText  {Str = 8}         -- "The Door is Locked"
  2:  Exit  {}

  3:  SetDoorState  {Id = 2, State = 1}
  4:  Subtract  {"Inventory", Value = 572}         -- "Cell Key"
end

event 3
      Hint = str[1]  -- "Door"
  0:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  1:  StatusText  {Str = 8}         -- "The Door is Locked"
  2:  Exit  {}

  3:  SetDoorState  {Id = 3, State = 1}
end

event 4
      Hint = str[1]  -- "Door"
  0:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  1:  StatusText  {Str = 8}         -- "The Door is Locked"
  2:  Exit  {}

  3:  SetDoorState  {Id = 4, State = 1}
end

event 5
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 5, State = 1}
end

event 6
      Hint = str[3]  -- "Double Door"
  0:  SetDoorState  {Id = 6, State = 1}
  1:  SetDoorState  {Id = 7, State = 1}
end

event 8
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 8, State = 1}
end

event 9
      Hint = str[1]  -- "Door"
  0:  Cmp  {"CurrentIntellect", Value = 40,   jump = 5}
  1:  ForPlayer  ("All")
  2:  DamagePlayer  {Player = "All", DamageType = const.Damage.Fire, Damage = 30}
  3:  StatusText  {Str = 14}         -- "You are not smart enough!"
  4:  Exit  {}

  5:  SetDoorState  {Id = 9, State = 1}
end

event 10
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 10, State = 1}
end

event 11
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 11, State = 1}
end

event 12
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 12, State = 1}
end

event 13
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 13, State = 1}
end

event 14
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 14, State = 1}
end

event 15
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 15, State = 1}
end

event 16
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 16, State = 1}
end

event 17
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 17, State = 1}
end

event 18
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 18, State = 1}
end

event 19
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 19, State = 1}
end

event 20
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 20, State = 1}
end

event 21
      Hint = str[1]  -- "Door"
  0:  Cmd1B  {unk_1 = 1, unk_2 = 1}
  1:  SetDoorState  {Id = 21, State = 1}
end

event 22
  0:  SetDoorState  {Id = 22, State = 1}
end

event 23
      Hint = str[3]  -- "Double Door"
  0:  SetDoorState  {Id = 23, State = 1}
  1:  SetDoorState  {Id = 24, State = 1}
end

event 25
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 25, State = 1}
end

event 26
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 26, State = 1}
end

event 27
      Hint = str[5]  -- "Cabinet"
  0:  OpenChest  {Id = 1}
  1:  Set  {"MapVar0", Value = 1}
  2:  Cmp  {"MapVar19", Value = 1,   jump = 8}
  3:  SummonMonsters  {TypeIndexInMapStats = 3, Level = 1, Count = 2, X = -1280, Y = 3200, Z = 30}
  4:  SummonMonsters  {TypeIndexInMapStats = 3, Level = 2, Count = 2, X = -1920, Y = 2688, Z = 30}
  5:  SummonMonsters  {TypeIndexInMapStats = 3, Level = 3, Count = 2, X = -2560, Y = 2944, Z = 30}
  6:  SummonMonsters  {TypeIndexInMapStats = 3, Level = 1, Count = 2, X = -1792, Y = 3584, Z = 0}
  7:  Set  {"MapVar19", Value = 1}
  8:  Exit  {}
end

event 28
      Hint = str[5]  -- "Cabinet"
  0:  OpenChest  {Id = 2}
end

event 29
      Hint = str[5]  -- "Cabinet"
  0:  OpenChest  {Id = 3}
end

event 30
      Hint = str[5]  -- "Cabinet"
  0:  OpenChest  {Id = 4}
end

event 31
      Hint = str[5]  -- "Cabinet"
  0:  OpenChest  {Id = 5}
end

event 32
      Hint = str[4]  -- "Chest"
  0:  OpenChest  {Id = 6}
end

event 33
      Hint = str[4]  -- "Chest"
  0:  OpenChest  {Id = 7}
end

event 34
      Hint = str[4]  -- "Chest"
  0:  OpenChest  {Id = 8}
end

event 35
      Hint = str[4]  -- "Chest"
  0:  OpenChest  {Id = 9}
end

event 36
      Hint = str[5]  -- "Cabinet"
  0:  OpenChest  {Id = 10}
end

event 37
      Hint = str[5]  -- "Cabinet"
  0:  OpenChest  {Id = 11}
end

event 39
      Hint = str[6]  -- "Bookshelves"
  0:  Cmd1C  {unk_1 = 1}
end

event 40
      Hint = str[6]  -- "Bookshelves"
  0:  StatusText  {Str = 7}         -- "You thumb through the books, but find nothing of interest."
  1:  FaceExpression  {Player = "Current", Frame = 39}
end

event 41
  0:  SummonMonsters  {TypeIndexInMapStats = 3, Level = 1, Count = 4, X = 10054, Y = 3290, Z = -511}
  1:  SummonMonsters  {TypeIndexInMapStats = 3, Level = 2, Count = 3, X = 10065, Y = 2200, Z = -511}
  2:  SummonMonsters  {TypeIndexInMapStats = 3, Level = 3, Count = 1, X = 10060, Y = 2220, Z = -511}
  3:  Exit  {}

  10: Cmp  {"QBits", Value = 1,   jump = 4}         --  1 D09, key to open D05
  11: SpeakNPC  {NPC = 66}         -- "Ghim Hammond"
  12: Add  {"Inventory", Value = 488}         -- "Key to Snergle's Chambers"
  13: Set  {"QBits", Value = 1}         --  1 D09, key to open D05
  14: Exit  {}
end

event 50
      Hint = str[9]  -- "Exit"
  0:  MoveToMap  {X = 16637, Y = -12300, Z = 705, Direction = 32, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 6, Name = "OutC2.Odm"}
end

event 60
  0:  OnRefillTimer  {Second = 1}
  1:  Set  {"MapVar49", Value = 20}
  2:  Set  {"MapVar50", Value = 20}
end

event 61
      Hint = str[10]  -- "Pool"
  0:  Cmp  {"MapVar49", Value = 1,   jump = 3}
  1:  StatusText  {Str = 13}         -- "Refreshing!"
  2:  GoTo  {jump = 6}

  3:  Subtract  {"MapVar49", Value = 1}
  4:  Add  {"HP", Value = 10}
  5:  StatusText  {Str = 11}         -- "+10 Hit points restored."
  6:  Set  {"AutonotesBits", Value = 94}         -- "10 Hit points cured by the right side pool in Snergle's Iron Mines."
end

event 62
      Hint = str[10]  -- "Pool"
  0:  Cmp  {"MapVar50", Value = 1,   jump = 3}
  1:  StatusText  {Str = 13}         -- "Refreshing!"
  2:  GoTo  {jump = 6}

  3:  Subtract  {"MapVar50", Value = 1}
  4:  Add  {"SP", Value = 10}
  5:  StatusText  {Str = 12}         -- "+10 Spell points restored."
  6:  Set  {"AutonotesBits", Value = 95}         -- "10 Spell points restored by the left side pool in Snergle's Iron Mines."
end

event 63
  0:  SetDoorState  {Id = 27, State = 1}
end

event 64
      Hint = str[6]  -- "Bookshelves"
  0:  Cmp  {"MapVar1", Value = 1,   jump = 3}
  1:  Add  {"Inventory", Value = 300}         -- "Torch Light"
  2:  Set  {"MapVar1", Value = 1}
  3:  Exit  {}
end

event 65
      Hint = str[6]  -- "Bookshelves"
  0:  Cmp  {"MapVar2", Value = 1,   jump = 3}
  1:  Add  {"Inventory", Value = 301}         -- "Flame Arrow"
  2:  Set  {"MapVar2", Value = 1}
  3:  Exit  {}
end

event 66
      Hint = str[6]  -- "Bookshelves"
  0:  Cmp  {"MapVar3", Value = 1,   jump = 3}
  1:  Add  {"Inventory", Value = 302}         -- "Protection from Fire"
  2:  Set  {"MapVar3", Value = 1}
  3:  Exit  {}
end

event 67
      Hint = str[6]  -- "Bookshelves"
  0:  Cmp  {"MapVar4", Value = 1,   jump = 3}
  1:  Add  {"Inventory", Value = 311}         -- "Wizard Eye"
  2:  Set  {"MapVar4", Value = 1}
  3:  Exit  {}
end

event 68
      Hint = str[6]  -- "Bookshelves"
  0:  Cmp  {"MapVar5", Value = 1,   jump = 3}
  1:  Add  {"Inventory", Value = 312}         -- "Static Charge"
  2:  Set  {"MapVar5", Value = 1}
  3:  Exit  {}
end

event 69
      Hint = str[6]  -- "Bookshelves"
  0:  Cmp  {"MapVar6", Value = 1,   jump = 3}
  1:  Add  {"Inventory", Value = 313}         -- "Protection from Elec"
  2:  Set  {"MapVar6", Value = 1}
  3:  Exit  {}
end

event 70
      Hint = str[6]  -- "Bookshelves"
  0:  Cmp  {"MapVar7", Value = 1,   jump = 3}
  1:  Add  {"Inventory", Value = 322}         -- "Awaken"
  2:  Set  {"MapVar7", Value = 1}
  3:  Exit  {}
end

event 71
      Hint = str[6]  -- "Bookshelves"
  0:  Cmp  {"MapVar8", Value = 1,   jump = 3}
  1:  Add  {"Inventory", Value = 323}         -- "Cold Beam"
  2:  Set  {"MapVar8", Value = 1}
  3:  Exit  {}
end

event 72
      Hint = str[6]  -- "Bookshelves"
  0:  Cmp  {"MapVar9", Value = 1,   jump = 3}
  1:  Add  {"Inventory", Value = 324}         -- "Protection from Cold"
  2:  Set  {"MapVar9", Value = 1}
  3:  Exit  {}
end

event 73
      Hint = str[6]  -- "Bookshelves"
  0:  Cmp  {"MapVar10", Value = 1,   jump = 3}
  1:  Add  {"Inventory", Value = 366}         -- "Cure Weakness"
  2:  Set  {"MapVar10", Value = 1}
  3:  Exit  {}
end

event 74
      Hint = str[6]  -- "Bookshelves"
  0:  Cmp  {"MapVar11", Value = 1,   jump = 3}
  1:  Add  {"Inventory", Value = 367}         -- "First Aid"
  2:  Set  {"MapVar11", Value = 1}
  3:  Exit  {}
end

event 75
      Hint = str[6]  -- "Bookshelves"
  0:  Cmp  {"MapVar12", Value = 1,   jump = 3}
  1:  Add  {"Inventory", Value = 368}         -- "Protection from Poison"
  2:  Set  {"MapVar12", Value = 1}
  3:  Exit  {}
end

event 76
      Hint = str[6]  -- "Bookshelves"
  0:  Cmp  {"MapVar13", Value = 1,   jump = 3}
  1:  Add  {"Inventory", Value = 348}         -- "Remove Curse"
  2:  Set  {"MapVar13", Value = 1}
  3:  Exit  {}
end

event 77
      Hint = str[5]  -- "Cabinet"
  0:  OpenChest  {Id = 12}
end
