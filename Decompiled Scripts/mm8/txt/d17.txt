str[0] = " "
str[1] = "Leave the dragon cave"
str[2] = "Door"
str[3] = "Chest"
str[4] = "Lever"
str[5] = "Button"
str[6] = ""
str[7] = ""
str[8] = ""
str[9] = "Bookshelf"
str[10] = ""
str[11] = ""
str[12] = ""
str[13] = ""
str[14] = "Cave Entrance"
str[15] = "The door is locked"
str[16] = "The cave is empty"
str[17] = ""
str[18] = ""
str[19] = ""
str[20] = ""
str[21] = ""
str[22] = ""
str[23] = ""
str[24] = ""
str[25] = ""
str[26] = ""
str[27] = ""
str[28] = ""
str[29] = ""
str[30] = "You have killed all of the Dragons"
str[31] = ""
str[32] = ""
str[33] = ""
str[34] = ""
str[35] = ""
str[36] = ""
str[37] = ""
str[38] = ""
str[39] = ""
str[40] = ""
str[41] = ""
str[42] = ""
str[43] = ""
str[44] = ""
str[45] = ""
str[46] = ""
str[47] = ""
str[48] = ""
str[49] = ""
str[50] = ""
str[51] = ""
str[52] = ""
str[53] = ""
str[54] = ""
str[55] = ""
str[56] = ""
str[57] = ""
str[58] = ""
str[59] = ""
str[60] = ""
str[61] = ""
str[62] = ""
str[63] = ""
str[64] = ""
str[65] = ""
str[66] = ""
str[67] = ""
str[68] = ""
str[69] = ""
str[70] = ""
str[71] = ""
str[72] = ""
str[73] = ""
str[74] = ""
str[75] = ""
str[76] = ""
str[77] = ""
str[78] = ""
str[79] = ""
str[80] = ""
str[81] = ""
str[82] = ""
str[83] = ""
str[84] = ""
str[85] = ""
str[86] = ""
str[87] = ""
str[88] = ""
str[89] = ""
str[90] = ""
str[91] = ""
str[92] = ""
str[93] = ""
str[94] = ""
str[95] = ""
str[96] = ""
str[97] = ""
str[98] = ""
str[99] = ""
str[100] = ""
str[101] = ""
str[102] = ""
str[103] = ""
str[104] = ""
str[105] = ""
str[106] = ""
str[107] = ""
str[108] = ""
str[109] = ""
str[110] = ""
str[111] = ""
str[112] = ""
str[113] = ""
str[114] = ""
str[115] = ""
str[116] = ""
str[117] = ""
str[118] = ""
str[119] = ""
str[120] = ""
str[121] = ""


event 1
      Hint = str[100]  -- ""

  0:  OnLoadMap  {}
  1:  Exit  {}
end

event 2
      Hint = str[100]  -- ""

  0:  OnLoadMap  {}
  1:  Exit  {}
end

event 3
      Hint = str[100]  -- ""

  0:  OnLoadMap  {}
  1:  Exit  {}
end

event 4
      Hint = str[100]  -- ""

  0:  OnLoadMap  {}
  1:  Exit  {}
end

event 5
      Hint = str[100]  -- ""

  0:  OnLoadMap  {}
  1:  Cmp  {"QBits", Value = 21,   jump = 12}         -- Allied with Charles Quioxte's Dragon Hunters. Return Dragon Egg to Quixote done.
  2:  Cmp  {"QBits", Value = 233,   jump = 11}         -- You have Pissed of the Dragons
  3:  Exit  {}

  4:  SetMonGroupBit  {NPCGroup = 44, Bit = const.MonsterBits.Hostile, On = false}         -- ""
  5:  SetMonGroupBit  {NPCGroup = 11, Bit = const.MonsterBits.Invisible, On = true}         -- "Misc Group for Riki(M1)"
  6:  SetMonGroupBit  {NPCGroup = 11, Bit = const.MonsterBits.Hostile, On = false}         -- "Misc Group for Riki(M1)"
  7:  Subtract  {"QBits", Value = 233}         -- You have Pissed of the Dragons
  8:  Set  {"MapVar9", Value = 0}
  9:  SetMonGroupBit  {NPCGroup = 45, Bit = const.MonsterBits.Hostile, On = false}         -- ""
  10: Exit  {}

  11: Cmp  {"Counter10", Value = 1344,   jump = 4}
  12: SetMonGroupBit  {NPCGroup = 44, Bit = const.MonsterBits.Hostile, On = true}         -- ""
  13: SetMonGroupBit  {NPCGroup = 11, Bit = const.MonsterBits.Invisible, On = false}         -- "Misc Group for Riki(M1)"
  14: SetMonGroupBit  {NPCGroup = 11, Bit = const.MonsterBits.Hostile, On = true}         -- "Misc Group for Riki(M1)"
  15: Set  {"MapVar9", Value = 2}
  16: SetMonGroupBit  {NPCGroup = 45, Bit = const.MonsterBits.Hostile, On = true}         -- ""
  17: Exit  {}
end

event 6
      Hint = str[100]  -- ""

  0:  OnLeaveMap  {}
  1:  Exit  {}
end

event 7
      Hint = str[100]  -- ""

  0:  OnLeaveMap  {}
  1:  Exit  {}
end

event 8
      Hint = str[100]  -- ""

  0:  OnLeaveMap  {}
  1:  Cmp  {"QBits", Value = 233,   jump = 7}         -- You have Pissed of the Dragons
  2:  Cmp  {"MapVar9", Value = 2,   jump = 5}
  3:  Set  {"MapVar9", Value = 0}
  4:  Exit  {}

  5:  Add  {"QBits", Value = 233}         -- You have Pissed of the Dragons
  6:  Set  {"Counter10", Value = 0}
  7:  Exit  {}
end

event 9
      Hint = str[100]  -- ""

  0:  OnLeaveMap  {}

  1:  OnTimer  {IntervalInHalfMinutes = 20}
  2:  Cmp  {"QBits", Value = 22,   jump = 25}         -- Allied with Dragons. Return Dragon Egg to Dragons done.
  3:  Cmp  {"QBits", Value = 155,   jump = 25}         -- Killed all Dragons in Garrote Gorge Area
  4:  CheckMonstersKilled  {CheckType = 2, Id = 189, Count = 0, InvisibleAsDead = 0,   jump(>=) = 6}
  5:  GoTo  {jump = 25}

  6:  CheckMonstersKilled  {CheckType = 2, Id = 190, Count = 0, InvisibleAsDead = 0,   jump(>=) = 8}
  7:  GoTo  {jump = 25}

  8:  CheckMonstersKilled  {CheckType = 2, Id = 191, Count = 0, InvisibleAsDead = 0,   jump(>=) = 10}
  9:  GoTo  {jump = 25}

  10: CheckMonstersKilled  {CheckType = 2, Id = 69, Count = 0, InvisibleAsDead = 0,   jump(>=) = 12}
  11: GoTo  {jump = 25}

  12: CheckMonstersKilled  {CheckType = 2, Id = 70, Count = 0, InvisibleAsDead = 0,   jump(>=) = 14}
  13: GoTo  {jump = 25}

  14: CheckMonstersKilled  {CheckType = 2, Id = 71, Count = 0, InvisibleAsDead = 0,   jump(>=) = 16}
  15: GoTo  {jump = 25}

  16: Cmp  {"QBits", Value = 156,   jump = 21}         -- Questbit set for Riki
  17: Set  {"QBits", Value = 156}         -- Questbit set for Riki
  18: SummonMonsters  {TypeIndexInMapStats = 2, Level = 1, Count = 223, X = -8, Y = 170, Z = 0, NPCGroup = 1, unk = 0}         -- "Peasants on Main Island of Dagger Wound"
  19: SetMonGroupBit  {NPCGroup = 1, Bit = const.MonsterBits.Invisible, On = true}         -- "Peasants on Main Island of Dagger Wound"
  20: GoTo  {jump = 25}

  21: Set  {"QBits", Value = 155}         -- Killed all Dragons in Garrote Gorge Area
  22: Add  {"QBits", Value = 225}         -- dead questbit for internal use(bling)
  23: Subtract  {"QBits", Value = 225}         -- dead questbit for internal use(bling)
  24: StatusText  {Str = 30}         -- "You have killed all of the Dragons"
  25: Exit  {}
end

event 10
      Hint = str[100]  -- ""

  0:  OnLeaveMap  {}
  1:  Cmp  {"QBits", Value = 22,   jump = 3}         -- Allied with Dragons. Return Dragon Egg to Dragons done.
  2:  GoTo  {jump = 5}

  3:  MoveNPC  {NPC = 21, HouseId = 0}         -- "Deftclaw Redreaver"
  4:  MoveNPC  {NPC = 66, HouseId = 175}         -- "Deftclaw Redreaver" -> "Council Chamber Door"
  5:  Exit  {}
end

event 81
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 0}
  1:  Exit  {}
end

event 82
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 1}
  1:  Exit  {}
end

event 83
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 2}
  1:  Exit  {}
end

event 84
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 3}
  1:  Exit  {}
end

event 85
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 4}
  1:  Exit  {}
end

event 86
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 5}
  1:  Exit  {}
end

event 87
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 6}
  1:  Exit  {}
end

event 88
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 7}
  1:  Exit  {}
end

event 89
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 8}
  1:  Exit  {}
end

event 90
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 9}
  1:  Exit  {}
end

event 91
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 10}
  1:  Exit  {}
end

event 92
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 11}
  1:  Exit  {}
end

event 93
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 12}
  1:  Exit  {}
end

event 94
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 13}
  1:  Exit  {}
end

event 95
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 14}
  1:  Exit  {}
end

event 96
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 15}
  1:  Exit  {}
end

event 97
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 16}
  1:  Exit  {}
end

event 98
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 17}
  1:  Exit  {}
end

event 99
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 18}
  1:  Exit  {}
end

event 100
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 19}
  1:  Exit  {}
end

event 131
      Hint = str[14]  -- "Cave Entrance"
  0:  EnterHouse  {Id = 350}         -- "Ishton's Cave"
  1:  Exit  {}
end

event 132
      Hint = str[14]  -- "Cave Entrance"
  0:  EnterHouse  {Id = 351}         -- "Ithilgore's Cave"
  1:  Exit  {}
end

event 133
      Hint = str[14]  -- "Cave Entrance"
  0:  EnterHouse  {Id = 352}         -- "Scarwing's Cave"
  1:  Exit  {}
end

event 201
      Hint = str[100]  -- ""
  0:  EnterHouse  {Id = 178}         -- "Dragon Leader's Cavern "
  1:  Exit  {}
end

event 451
      Hint = str[100]  -- ""
  0:  Cmp  {"Invisible", Value = 0,   jump = 4}
  1:  Cmp  {"MapVar9", Value = 1,   jump = 4}
  2:  SpeakNPC  {NPC = 39}         -- "Guard"
  3:  Set  {"MapVar9", Value = 1}
  4:  Exit  {}
end

event 452
      Hint = str[100]  -- ""
  0:  Cmp  {"MapVar9", Value = 2,   jump = 2}
  1:  Set  {"MapVar9", Value = 0}
  2:  Exit  {}
end

event 453
      Hint = str[100]  -- ""
  0:  Cmp  {"MapVar9", Value = 2,   jump = 6}
  1:  SetMonGroupBit  {NPCGroup = 44, Bit = const.MonsterBits.Hostile, On = true}         -- ""
  2:  SetMonGroupBit  {NPCGroup = 11, Bit = const.MonsterBits.Invisible, On = false}         -- "Misc Group for Riki(M1)"
  3:  SetMonGroupBit  {NPCGroup = 11, Bit = const.MonsterBits.Hostile, On = true}         -- "Misc Group for Riki(M1)"
  4:  Set  {"MapVar9", Value = 2}
  5:  SetMonGroupBit  {NPCGroup = 45, Bit = const.MonsterBits.Hostile, On = true}         -- ""
  6:  Exit  {}
end

event 501
      Hint = str[1]  -- "Leave the dragon cave"
  0:  MoveToMap  {X = 6376, Y = 12420, Z = 1616, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 1, Name = "Out05.odm"}
  1:  Exit  {}
end
