str[0] = " "
str[1] = "Door"
str[2] = "Enter the tunnel."
str[3] = "Chest"
str[4] = "Button"
str[5] = "Lever"
str[6] = "Vault"
str[7] = "Cabinet"
str[8] = "Switch"
str[9] = "The Steel Grate is locked."
str[10] = "Bookcase"
str[11] = "Exit to Celeste"
str[12] = "You can't exit without the Control Cube"
str[13] = ""
str[14] = "You Successfully disarm the trap"
str[15] = ""
str[16] = "Take a Drink"
str[17] = "Not Very Refreshing"
str[18] = "Refreshing"
str[19] = ""
str[20] = ""
str[21] = ""
str[22] = ""
str[23] = ""
str[24] = ""
str[25] = ""
str[26] = ""
str[27] = ""
str[28] = ""
str[29] = ""
str[30] = ""
str[31] = ""
str[32] = ""
str[33] = ""
str[34] = ""
str[35] = ""
str[36] = ""
str[37] = ""
str[38] = ""
str[39] = ""
str[40] = ""
str[41] = ""
str[42] = ""
str[43] = ""
str[44] = ""
str[45] = ""
str[46] = ""
str[47] = ""
str[48] = ""
str[49] = ""
str[50] = ""
str[51] = ""
str[52] = ""
str[53] = ""
str[54] = ""
str[55] = ""
str[56] = ""
str[57] = ""
str[58] = ""
str[59] = ""
str[60] = ""
str[61] = ""
str[62] = ""
str[63] = ""
str[64] = ""
str[65] = ""
str[66] = ""
str[67] = ""
str[68] = ""
str[69] = ""
str[70] = ""
str[71] = ""
str[72] = ""
str[73] = ""
str[74] = ""
str[75] = ""
str[76] = ""
str[77] = ""
str[78] = ""
str[79] = ""
str[80] = ""
str[81] = ""
str[82] = ""
str[83] = ""
str[84] = ""
str[85] = ""
str[86] = ""
str[87] = ""
str[88] = ""
str[89] = ""
str[90] = ""
str[91] = ""
str[92] = ""
str[93] = ""
str[94] = ""
str[95] = ""
str[96] = ""
str[97] = ""
str[98] = ""
str[99] = ""
str[100] = ""
str[101] = ""
str[102] = ""
str[103] = ""
str[104] = ""
str[105] = ""
str[106] = ""
str[107] = ""
str[108] = ""
str[109] = ""
str[110] = ""
str[111] = ""
str[112] = ""
str[113] = ""
str[114] = ""
str[115] = ""
str[116] = ""
str[117] = ""
str[118] = ""
str[119] = ""
str[120] = ""
str[121] = ""


event 1
      Hint = str[100]  -- ""

  0:  OnLoadMap  {}
  1:  ForPlayer  ("All")
  2:  SetMonGroupBit  {NPCGroup = 6, Bit = const.MonsterBits.Hostile, On = false}         -- "Group for M1"
  3:  SetMonGroupBit  {NPCGroup = 7, Bit = const.MonsterBits.Hostile, On = false}         -- "Group fo M2"
  4:  SetMonGroupBit  {NPCGroup = 8, Bit = const.MonsterBits.Hostile, On = false}         -- "Group for M3"
  5:  SetMonGroupBit  {NPCGroup = 9, Bit = const.MonsterBits.Hostile, On = false}         -- "Group for Malwick's Assc."
  6:  SetMonGroupBit  {NPCGroup = 10, Bit = const.MonsterBits.Hostile, On = false}         -- "Southern Village Group in Harmondy"
  7:  SetMonGroupBit  {NPCGroup = 11, Bit = const.MonsterBits.Hostile, On = false}         -- "Main village in Harmondy"
  8:  Cmp  {"QBits", Value = 380,   jump = 10}         -- "Pass the Test of Friendship"
  9:  GoTo  {jump = 16}

  10: SetNPCGreeting  {NPC = 456, Greeting = 41}         -- "The Coding Wizard" : "Friends do not abandon friends in the midst of a fray!  You have failed the Test of Friendship!"
  11: SpeakNPC  {NPC = 456}         -- "The Coding Wizard"
  12: Set  {"Awards", Value = 33}         -- "Hall of Shame Award �Unfaithful Friends�"
  13: Subtract  {"Inventory", Value = 675}         -- "Control Cube"
  14: Set  {"Eradicated", Value = 0}
  15: Exit  {}

  16: SetNPCTopic  {NPC = 456, Index = 1, Event = 0}         -- "The Coding Wizard"
  17: SetNPCTopic  {NPC = 456, Index = 2, Event = 0}         -- "The Coding Wizard"
  18: SetNPCTopic  {NPC = 456, Index = 3, Event = 0}         -- "The Coding Wizard"
  19: SetNPCTopic  {NPC = 18, Index = 0, Event = 0}         -- "Lord Godwinson"
  20: SetNPCTopic  {NPC = 18, Index = 1, Event = 0}         -- "Lord Godwinson"
  21: SetNPCTopic  {NPC = 21, Index = 0, Event = 0}         -- "Zedd True Shot"
  22: SetNPCTopic  {NPC = 21, Index = 1, Event = 0}         -- "Zedd True Shot"
  23: SetNPCTopic  {NPC = 18, Index = 2, Event = 0}         -- "Lord Godwinson"
  24: SetNPCTopic  {NPC = 456, Index = 0, Event = 571}         -- "The Coding Wizard" : "SAVE you Game!"
  25: Exit  {}
end

event 2
      Hint = str[100]  -- ""

  0:  OnLeaveMap  {}
  1:  Cmp  {"QBits", Value = 227,   jump = 3}         -- Dragon Egg - I lost it
  2:  Cmp  {"Inventory", Value = 647,   jump = 4}         -- "Dragon Egg"
  3:  Exit  {}

  4:  Add  {"QBits", Value = 227}         -- Dragon Egg - I lost it
end

event 176
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 1}
end

event 177
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 2}
end

event 178
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 3}
end

event 179
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 4}
end

event 180
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 5}
end

event 181
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 6}
end

event 182
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 7}
end

event 183
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 8}
end

event 184
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 9}
end

event 185
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 10}
end

event 186
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 11}
end

event 187
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 12}
end

event 188
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 13}
end

event 189
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 14}
end

event 190
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 15}
end

event 191
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 16}
end

event 192
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 17}
end

event 193
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 18}
end

event 194
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 19}
end

event 195
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 0}
end

event 501
      Hint = str[2]  -- "Enter the tunnel."
  0:  ForPlayer  ("All")
  1:  Cmp  {"QBits", Value = 379,   jump = 22}         -- Exit 1-time Cave 1 Vault
  2:  Cmp  {"Inventory", Value = 161,   jump = 5}         -- "Grate Key"
  3:  StatusText  {Str = 9}         -- "The Steel Grate is locked."
  4:  Exit  {}

  5:  Subtract  {"Inventory", Value = 161}         -- "Grate Key"
  6:  SetNPCGreeting  {NPC = 456, Greeting = 35}         --[[ "The Coding Wizard" : "You are finally face-to-face with Jester�s Folly and his minions, adventurers.  It is now time for the Test of Friendship.  Hear me and heed my words.

I give you a solemn charge, adventurers.  It is your responsibility to ensure that the entire brotherhood survives this encounter.  All must leave this cave together, or you will have failed the Test of Friendship and, hence, The Game!

Once you have defeated Jester�s Folly and possess the Cube, you must gather each member of the fellowship and then use the cave exit immediately behind you to return to Celeste. Return to Robert the Wise with the Cube and in company with the entire brotherhood.  Robert will grant you the appropriate rewards for your victory!

But be warned, adventurers!  If you let any of the brotherhood fall in battle, you will be eradicated, returned to Hamdondale, and branded �Unfaithful Friends�.  Unfaithful friends will not be able to complete �The Game�." ]]
  7:  SpeakNPC  {NPC = 456}         -- "The Coding Wizard"
  8:  SetNPCGreeting  {NPC = 21, Greeting = 36}         -- "Zedd True Shot" : "What a glorious day for victory, my friends!."
  9:  SetNPCGreeting  {NPC = 37, Greeting = 36}         -- "Pascal the Mad Mage" : "What a glorious day for victory, my friends!."
  10: SetNPCGreeting  {NPC = 34, Greeting = 36}         -- "Duke Bimbasto" : "What a glorious day for victory, my friends!."
  11: SetNPCGreeting  {NPC = 35, Greeting = 36}         -- "Sir Vilx of Stone City" : "What a glorious day for victory, my friends!."
  12: SetNPCGreeting  {NPC = 20, Greeting = 36}         -- "Baron BunGleau" : "What a glorious day for victory, my friends!."
  13: SetNPCGreeting  {NPC = 18, Greeting = 36}         -- "Lord Godwinson" : "What a glorious day for victory, my friends!."
  14: Set  {"NPCs", Value = 21}         -- "Zedd True Shot"
  15: Set  {"NPCs", Value = 18}         -- "Lord Godwinson"
  16: Set  {"NPCs", Value = 20}         -- "Baron BunGleau"
  17: Set  {"NPCs", Value = 35}         -- "Sir Vilx of Stone City"
  18: Set  {"NPCs", Value = 34}         -- "Duke Bimbasto"
  19: Set  {"NPCs", Value = 37}         -- "Pascal the Mad Mage"
  20: Set  {"QBits", Value = 379}         -- Exit 1-time Cave 1 Vault
  21: Exit  {}

  22: SpeakNPC  {NPC = 456}         -- "The Coding Wizard"
  23: Set  {"QBits", Value = 380}         -- "Pass the Test of Friendship"
  24: Subtract  {"NPCs", Value = 21}         -- "Zedd True Shot"
  25: Subtract  {"NPCs", Value = 18}         -- "Lord Godwinson"
  26: Subtract  {"NPCs", Value = 20}         -- "Baron BunGleau"
  27: Subtract  {"NPCs", Value = 35}         -- "Sir Vilx of Stone City"
  28: Subtract  {"NPCs", Value = 34}         -- "Duke Bimbasto"
  29: Subtract  {"NPCs", Value = 37}         -- "Pascal the Mad Mage"
  30: SetMonGroupBit  {NPCGroup = 6, Bit = const.MonsterBits.Hostile, On = false}         -- "Group for M1"
  31: SetMonGroupBit  {NPCGroup = 7, Bit = const.MonsterBits.Hostile, On = false}         -- "Group fo M2"
  32: SetMonGroupBit  {NPCGroup = 8, Bit = const.MonsterBits.Hostile, On = false}         -- "Group for M3"
  33: SetMonGroupBit  {NPCGroup = 9, Bit = const.MonsterBits.Hostile, On = false}         -- "Group for Malwick's Assc."
  34: SetMonGroupBit  {NPCGroup = 10, Bit = const.MonsterBits.Hostile, On = false}         -- "Southern Village Group in Harmondy"
  35: SetMonGroupBit  {NPCGroup = 11, Bit = const.MonsterBits.Hostile, On = false}         -- "Main village in Harmondy"
  36: SetMonGroupBit  {NPCGroup = 6, Bit = const.MonsterBits.Invisible, On = false}         -- "Group for M1"
  37: SetMonGroupBit  {NPCGroup = 7, Bit = const.MonsterBits.Invisible, On = false}         -- "Group fo M2"
  38: SetMonGroupBit  {NPCGroup = 8, Bit = const.MonsterBits.Invisible, On = false}         -- "Group for M3"
  39: SetMonGroupBit  {NPCGroup = 9, Bit = const.MonsterBits.Invisible, On = false}         -- "Group for Malwick's Assc."
  40: SetMonGroupBit  {NPCGroup = 10, Bit = const.MonsterBits.Invisible, On = false}         -- "Southern Village Group in Harmondy"
  41: SetMonGroupBit  {NPCGroup = 11, Bit = const.MonsterBits.Invisible, On = false}         -- "Main village in Harmondy"
  42: SetMonGroupBit  {NPCGroup = 5, Bit = const.MonsterBits.Hostile, On = true}         -- "Generic Monster Group for Dungeons"
  43: SetNPCTopic  {NPC = 21, Index = 1, Event = 40}         -- "Zedd True Shot" : "Let's Go!"
  44: SetNPCTopic  {NPC = 34, Index = 1, Event = 126}         -- "Duke Bimbasto" : "Let's Go!"
  45: SetNPCTopic  {NPC = 35, Index = 1, Event = 127}         -- "Sir Vilx of Stone City" : "Let's Go!"
  46: SetNPCTopic  {NPC = 37, Index = 1, Event = 134}         -- "Pascal the Mad Mage" : "Let's Go!"
  47: SetNPCTopic  {NPC = 20, Index = 1, Event = 135}         -- "Baron BunGleau" : "Let's Go!"
  48: SetNPCTopic  {NPC = 18, Index = 1, Event = 136}         -- "Lord Godwinson" : "Let's Go!"
  49: SetNPCTopic  {NPC = 456, Index = 0, Event = 424}         -- "The Coding Wizard" : "A word of Caution!"
  50: MoveToMap  {X = -54, Y = 3470, Z = 0, Direction = 1536, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  51: Exit  {}
end

event 502
      Hint = str[11]  -- "Exit to Celeste"
  0:  ForPlayer  ("All")
  1:  Cmp  {"Inventory", Value = 675,   jump = 4}         -- "Control Cube"
  2:  StatusText  {Str = 12}         -- "You can't exit without the Control Cube"
  3:  Exit  {}

  4:  Cmp  {"NPCs", Value = 34,   jump = 6}         -- "Duke Bimbasto"
  5:  GoTo  {jump = 20}

  6:  Cmp  {"NPCs", Value = 35,   jump = 8}         -- "Sir Vilx of Stone City"
  7:  GoTo  {jump = 20}

  8:  Cmp  {"NPCs", Value = 18,   jump = 10}         -- "Lord Godwinson"
  9:  GoTo  {jump = 20}

  10: Cmp  {"NPCs", Value = 37,   jump = 12}         -- "Pascal the Mad Mage"
  11: GoTo  {jump = 20}

  12: Cmp  {"NPCs", Value = 20,   jump = 14}         -- "Baron BunGleau"
  13: GoTo  {jump = 20}

  14: Cmp  {"NPCs", Value = 21,   jump = 16}         -- "Zedd True Shot"
  15: GoTo  {jump = 20}

  16: Set  {"Awards", Value = 32}         -- "Declared Friends of �The Game�"
  17: Subtract  {"QBits", Value = 380}         -- "Pass the Test of Friendship"
  18: MoveToMap  {X = -6781, Y = 792, Z = 57, Direction = 512, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "D25.blv"}
  19: Exit  {}

  20: Set  {"Awards", Value = 33}         -- "Hall of Shame Award �Unfaithful Friends�"
  21: Subtract  {"Inventory", Value = 675}         -- "Control Cube"
  22: Set  {"Eradicated", Value = 0}
  23: Exit  {}
end
