str[0] = " "
str[1] = "Door"
str[2] = "Lever"
str[3] = "Shelves"
str[4] = "You found something!"
str[5] = "Nothing here"
str[6] = "Not enough gold"
str[7] = "Potions for sale"
str[8] = "Bad Food!"
str[9] = "Hic!"
str[10] = "Pantry"
str[11] = "Counter"
str[12] = "Desk"
str[13] = "New World Computing"
str[14] = "Exit"
str[15] = ""


event 1
      Hint = str[1]  -- "Door"
      MazeInfo = str[13]  -- "New World Computing"
  0:  SetDoorState  {Id = 1, State = 1}
end

event 2
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 2, State = 1}
end

event 3
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 3, State = 1}
end

event 4
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 4, State = 1}
end

event 5
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 5, State = 1}
end

event 6
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 6, State = 1}
end

event 7
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 7, State = 1}
  1:  SetDoorState  {Id = 8, State = 0}
  2:  SetDoorState  {Id = 41, State = 0}
end

event 8
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 8, State = 1}
  1:  SetDoorState  {Id = 7, State = 0}
  2:  SetDoorState  {Id = 9, State = 0}
end

event 9
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 9, State = 1}
  1:  SetDoorState  {Id = 8, State = 0}
  2:  SetDoorState  {Id = 11, State = 0}
end

event 10
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 10, State = 1}
end

event 11
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 11, State = 1}
  1:  SetDoorState  {Id = 12, State = 0}
  2:  SetDoorState  {Id = 9, State = 0}
  3:  SetDoorState  {Id = 17, State = 0}
end

event 12
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 12, State = 1}
  1:  SetDoorState  {Id = 17, State = 0}
  2:  SetDoorState  {Id = 11, State = 0}
  3:  SetDoorState  {Id = 17, State = 0}
end

event 13
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 13, State = 1}
  1:  SetDoorState  {Id = 17, State = 0}
end

event 14
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 14, State = 1}
end

event 15
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 15, State = 1}
end

event 17
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 17, State = 1}
  1:  SetDoorState  {Id = 12, State = 0}
  2:  SetDoorState  {Id = 18, State = 0}
end

event 18
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 18, State = 1}
  1:  SetDoorState  {Id = 17, State = 0}
  2:  SetDoorState  {Id = 19, State = 0}
end

event 19
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 19, State = 1}
  1:  SetDoorState  {Id = 18, State = 0}
  2:  SetDoorState  {Id = 20, State = 0}
  3:  SetDoorState  {Id = 17, State = 0}
end

event 20
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 20, State = 1}
  1:  SetDoorState  {Id = 19, State = 0}
  2:  SetDoorState  {Id = 21, State = 0}
  3:  SetDoorState  {Id = 17, State = 0}
end

event 21
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 21, State = 1}
  1:  SetDoorState  {Id = 20, State = 0}
  2:  SetDoorState  {Id = 22, State = 0}
  3:  SetDoorState  {Id = 17, State = 0}
end

event 22
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 22, State = 1}
  1:  SetDoorState  {Id = 21, State = 0}
  2:  SetDoorState  {Id = 23, State = 0}
  3:  SetDoorState  {Id = 17, State = 0}
end

event 23
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 23, State = 1}
  1:  SetDoorState  {Id = 22, State = 0}
  2:  SetDoorState  {Id = 24, State = 0}
end

event 24
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 24, State = 1}
  1:  SetDoorState  {Id = 23, State = 0}
  2:  SetDoorState  {Id = 25, State = 0}
end

event 25
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 25, State = 1}
  1:  SetDoorState  {Id = 24, State = 0}
  2:  SetDoorState  {Id = 26, State = 0}
end

event 26
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 26, State = 1}
  1:  SetDoorState  {Id = 25, State = 0}
  2:  SetDoorState  {Id = 27, State = 0}
end

event 27
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 27, State = 1}
  1:  SetDoorState  {Id = 28, State = 0}
  2:  SetDoorState  {Id = 26, State = 0}
end

event 28
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 28, State = 1}
  1:  SetDoorState  {Id = 27, State = 0}
  2:  SetDoorState  {Id = 29, State = 0}
end

event 29
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 29, State = 1}
  1:  SetDoorState  {Id = 28, State = 0}
  2:  SetDoorState  {Id = 30, State = 0}
end

event 30
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 30, State = 1}
  1:  SetDoorState  {Id = 31, State = 0}
  2:  SetDoorState  {Id = 29, State = 0}
end

event 31
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 31, State = 1}
  1:  SetDoorState  {Id = 30, State = 0}
  2:  SetDoorState  {Id = 32, State = 0}
end

event 32
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 32, State = 1}
  1:  SetDoorState  {Id = 31, State = 0}
  2:  SetDoorState  {Id = 33, State = 0}
end

event 33
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 33, State = 1}
  1:  SetDoorState  {Id = 34, State = 0}
  2:  SetDoorState  {Id = 32, State = 0}
end

event 34
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 34, State = 1}
  1:  SetDoorState  {Id = 38, State = 0}
  2:  SetDoorState  {Id = 33, State = 0}
end

event 35
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 35, State = 1}
end

event 36
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 36, State = 1}
end

event 37
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 37, State = 1}
end

event 38
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 38, State = 1}
  1:  SetDoorState  {Id = 39, State = 0}
  2:  SetDoorState  {Id = 34, State = 0}
end

event 39
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 39, State = 1}
end

event 40
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 40, State = 1}
  1:  SetDoorState  {Id = 39, State = 0}
  2:  SetDoorState  {Id = 41, State = 0}
end

event 41
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 41, State = 1}
  1:  SetDoorState  {Id = 7, State = 0}
  2:  SetDoorState  {Id = 40, State = 0}
end

event 42
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 42, State = 2}         -- switch state
  1:  SetDoorState  {Id = 16, State = 2}         -- switch state
end

event 44
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 44, State = 1}
  1:  SetDoorState  {Id = 43, State = 1}
end

event 45
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 45, State = 1}
end

event 46
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 46, State = 1}
end

event 47
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 47, State = 1}
end

event 48
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 48, State = 1}
end

event 49
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 49, State = 1}
end

event 50
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 50, State = 1}
end

event 51
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 51, State = 1}
end

event 52
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 52, State = 1}
end

event 53
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 53, State = 1}
end

event 54
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 54, State = 1}
end

event 55
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 55, State = 1}
end

event 56
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 56, State = 1}
end

event 57
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 57, State = 1}
end

event 58
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 58, State = 1}
end

event 59
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 59, State = 1}
end

event 60
      Hint = str[3]  -- "Shelves"
  0:  Cmp  {"MapVar0", Value = 4,   jump = 16}
  1:  Add  {"MapVar0", Value = 1}
  2:  StatusText  {Str = 4}         -- "You found something!"
  3:  RandomGoTo  {jumpA = 4, jumpB = 6, jumpC = 8, jumpD = 10, jumpE = 12, jumpF = 14}
  4:  Add  {"Inventory", Value = 1}         -- "Longsword"
  5:  Exit  {}

  6:  Add  {"Inventory", Value = 15}         -- "Dagger"
  7:  Exit  {}

  8:  Add  {"Inventory", Value = 58}         -- "Club"
  9:  Exit  {}

  10: Add  {"Inventory", Value = 161}         -- "Phirna Root"
  11: Exit  {}

  12: Add  {"Inventory", Value = 309}         -- "Inferno"
  13: Exit  {}

  14: Add  {"Inventory", Value = 94}         -- "Cloth Hat"
  15: Exit  {}

  16: StatusText  {Str = 5}         -- "Nothing here"
end

event 61
      Hint = str[7]  -- "Potions for sale"
  0:  Cmp  {"Gold", Value = 100,   jump = 3}
  1:  StatusText  {Str = 6}         -- "Not enough gold"
  2:  Exit  {}

  3:  StatusText  {Str = 4}         -- "You found something!"
  4:  RandomGoTo  {jumpA = 5, jumpB = 8, jumpC = 11, jumpD = 14, jumpE = 17, jumpF = 20}
  5:  Add  {"Inventory", Value = 163}         -- "Potion Bottle"
  6:  Subtract  {"Gold", Value = 50}
  7:  Exit  {}

  8:  Add  {"Inventory", Value = 164}         -- "Cure Wounds"
  9:  Subtract  {"Gold", Value = 20}
  10: Exit  {}

  11: Add  {"Inventory", Value = 164}         -- "Cure Wounds"
  12: Subtract  {"Gold", Value = 100}
  13: Exit  {}

  14: Add  {"Inventory", Value = 165}         -- "Magic Potion"
  15: Subtract  {"Gold", Value = 50}
  16: Exit  {}

  17: Add  {"Inventory", Value = 166}         -- "Energy"
  18: Subtract  {"Gold", Value = 20}
  19: Exit  {}

  20: Add  {"Inventory", Value = 164}         -- "Cure Wounds"
  21: Subtract  {"Gold", Value = 50}
  22: Exit  {}
end

event 62
      Hint = str[10]  -- "Pantry"
  0:  RandomGoTo  {jumpA = 1, jumpB = 3, jumpC = 5, jumpD = 7, jumpE = 10, jumpF = 13}
  1:  Add  {"Food", Value = 1}
  2:  Exit  {}

  3:  Add  {"Food", Value = 2}
  4:  Exit  {}

  5:  Add  {"Food", Value = 1}
  6:  Exit  {}

  7:  StatusText  {Str = 8}         -- "Bad Food!"
  8:  Set  {"PoisonedGreen", Value = 1}
  9:  Exit  {}

  10: StatusText  {Str = 9}         -- "Hic!"
  11: Set  {"Drunk", Value = 1}
  12: Exit  {}

  13: Add  {"Inventory", Value = 163}         -- "Potion Bottle"
end

event 63
      Hint = str[11]  -- "Counter"
  0:  Add  {"Inventory", Value = 163}         -- "Potion Bottle"
end

event 64
  0:  Cmp  {"MapVar1", Value = 16,   jump = 9}
  1:  Add  {"MapVar1", Value = 1}
  2:  StatusText  {Str = 4}         -- "You found something!"
  3:  RandomGoTo  {jumpA = 4, jumpB = 6, jumpC = 8, jumpD = 0, jumpE = 0, jumpF = 0}
  4:  Add  {"Inventory", Value = 160}         -- "Poppysnaps"
  5:  Exit  {}

  6:  Add  {"Inventory", Value = 161}         -- "Phirna Root"
  7:  Exit  {}

  8:  Add  {"Inventory", Value = 162}         -- "Widoweeps Berries"
  9:  Exit  {}
end

event 65
  0:  OnLoadMap  {}
  1:  SummonObject  {Type = 36, X = 2944, Y = 1920, Z = 1, Speed = 1000, Count = 5, RandomAngle = false}         -- not found!
end

event 66
      Hint = str[12]  -- "Desk"
  0:  Cmp  {"MapVar3", Value = 1,   jump = 11}
  1:  Add  {"MapVar3", Value = 1}
  2:  StatusText  {Str = 4}         -- "You found something!"
  3:  RandomGoTo  {jumpA = 4, jumpB = 6, jumpC = 8, jumpD = 10, jumpE = 12, jumpF = 0}
  4:  Add  {"Inventory", Value = 71}         -- "Chain Mail"
  5:  Exit  {}

  6:  Add  {"Inventory", Value = 72}         -- "Steel Chain Mail"
  7:  Exit  {}

  8:  Add  {"Inventory", Value = 73}         -- "Noble Chain Mail"
  9:  Exit  {}

  10: Add  {"Inventory", Value = 74}         -- "Royal Chain Mail"
  11: Exit  {}

  12: Add  {"Inventory", Value = 75}         -- "Majestic Chain Mail"
end

event 67
  0:  ForPlayer  ("All")
  1:  Add  {"Afraid", Value = 10}
end

event 68
  0:  ForPlayer  ("All")
  1:  Cmp  {"Afraid", Value = 10,   jump = 3}
  2:  Exit  {}

  3:  Subtract  {"Afraid", Value = 10}
end

event 69
      Hint = str[12]  -- "Desk"
  0:  Cmp  {"MapVar2", Value = 1,   jump = 4}
  1:  Set  {"MapVar2", Value = 1}
  2:  StatusText  {Str = 4}         -- "You found something!"
  3:  Add  {"Inventory", Value = 117}         -- "Armored Boots"
  4:  Exit  {}
end

event 70
      Hint = str[12]  -- "Desk"
  0:  Cmp  {"MapVar4", Value = 1,   jump = 4}
  1:  Set  {"MapVar4", Value = 1}
  2:  StatusText  {Str = 4}         -- "You found something!"
  3:  Add  {"Inventory", Value = 58}         -- "Club"
  4:  Exit  {}
end

event 71
      Hint = str[12]  -- "Desk"
  0:  Cmd14  {unk_1 = 71, unk_2 = 1}
  1:  Cmd14  {unk_1 = 72, unk_2 = 1}
  2:  Cmd14  {unk_1 = 73, unk_2 = 1}
  3:  Cmd14  {unk_1 = 74, unk_2 = 1}
  4:  Cmd14  {unk_1 = 75, unk_2 = 1}
  5:  Cmd14  {unk_1 = 117, unk_2 = 1}
  6:  Cmd14  {unk_1 = 58, unk_2 = 1}
end

event 72
      Hint = str[12]  -- "Desk"
  0:  Cmp  {"MapVar5", Value = 4,   jump = 4}
  1:  StatusText  {Str = 4}         -- "You found something!"
  2:  Add  {"Inventory", Value = 106}         -- "Phantom Cloak"
  3:  Add  {"MapVar5", Value = 1}
  4:  Exit  {}
end

event 73
      Hint = str[12]  -- "Desk"
  0:  CastSpell  {Spell = 83, Mastery = const.Master, Skill = 64, FromX = 0, FromY = 0, FromZ = 0, ToX = 0, ToY = 0, ToZ = 0}         -- "Day of the Gods"
end

event 74
      Hint = str[12]  -- "Desk"
  0:  Cmp  {"Gold", Value = 10000,   jump = 3}
  1:  Add  {"Gold", Value = 500}
  2:  StatusText  {Str = 4}         -- "You found something!"
  3:  Exit  {}
end

event 75
      Hint = str[14]  -- "Exit"
  0:  MoveToMap  {X = 12808, Y = 6832, Z = 64, Direction = 512, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "OutB3.Odm"}
end
