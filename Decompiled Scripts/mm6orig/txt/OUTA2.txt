str[0] = " "
str[1] = "Drink from Fountain"
str[2] = "+100 Power temporary."
str[3] = "Refreshing!"
str[4] = "Chest"
str[5] = "Paradise Valley"
str[6] = "The surface of the obelisk is blood warm to the touch.  A message swims into view as you remove your hand:                                                                                                                                                            nhrh_aherheatvdi"
str[7] = "Obelisk"
str[8] = ""


event 2
      Hint = str[11]
      MazeInfo = str[5]  -- "Paradise Valley"
  0:  EnterHouse  {Id = 11}         -- "Singing Steel"
end

event 3
      Hint = str[11]
  0:  Exit  {}

  1:  EnterHouse  {Id = 11}         -- "Singing Steel"
end

event 4
      Hint = str[28]
  0:  EnterHouse  {Id = 28}         -- "The Enchanted Hauberk"
end

event 5
      Hint = str[28]
  0:  Exit  {}

  1:  EnterHouse  {Id = 28}         -- "The Enchanted Hauberk"
end

event 6
      Hint = str[41]
  0:  EnterHouse  {Id = 41}         -- "Mighty Magicks"
end

event 7
      Hint = str[41]
  0:  Exit  {}

  1:  EnterHouse  {Id = 41}         -- "Mighty Magicks"
end

event 8
      Hint = str[82]
  0:  EnterHouse  {Id = 82}         -- "The Sparring Ground"
end

event 9
      Hint = str[82]
  0:  Exit  {}

  1:  EnterHouse  {Id = 82}         -- "The Sparring Ground"
end

event 10
      Hint = str[112]
  0:  EnterHouse  {Id = 112}         -- "The Last Chance"
end

event 11
      Hint = str[112]
  0:  Exit  {}

  1:  EnterHouse  {Id = 112}         -- "The Last Chance"
end

event 50
      Hint = str[211]
  0:  EnterHouse  {Id = 211}         -- "House"
end

event 51
      Hint = str[212]
  0:  EnterHouse  {Id = 212}         -- "House"
end

event 52
      Hint = str[213]
  0:  EnterHouse  {Id = 213}         -- "House"
end

event 53
      Hint = str[214]
  0:  EnterHouse  {Id = 214}         -- "House"
end

event 54
      Hint = str[215]
  0:  EnterHouse  {Id = 215}         -- "House"
end

event 55
      Hint = str[216]
  0:  EnterHouse  {Id = 216}         -- "House"
end

event 56
      Hint = str[217]
  0:  EnterHouse  {Id = 217}         -- "House"
end

event 57
      Hint = str[218]
  0:  EnterHouse  {Id = 218}         -- "House"
end

event 58
      Hint = str[219]
  0:  EnterHouse  {Id = 219}         -- "House"
end

event 59
      Hint = str[220]
  0:  EnterHouse  {Id = 220}         -- "House"
end

event 60
      Hint = str[221]
  0:  EnterHouse  {Id = 221}         -- "House"
end

event 61
      Hint = str[222]
  0:  EnterHouse  {Id = 222}         -- "House"
end

event 62
      Hint = str[223]
  0:  EnterHouse  {Id = 223}         -- "House"
end

event 63
      Hint = str[224]
  0:  EnterHouse  {Id = 224}         -- "House"
end

event 64
      Hint = str[225]
  0:  EnterHouse  {Id = 225}         -- "House"
end

event 65
      Hint = str[226]
  0:  EnterHouse  {Id = 226}         -- "House"
      Hint = str[227]
  0:  EnterHouse  {Id = 227}         -- "House"
end

event 75
      Hint = str[4]  -- "Chest"
  0:  OpenChest  {Id = 1}
end

event 76
      Hint = str[4]  -- "Chest"
  0:  OpenChest  {Id = 2}
end

event 77
      Hint = str[4]  -- "Chest"
  0:  OpenChest  {Id = 3}
end

event 90
      Hint = str[78]
  0:  EnterHouse  {Id = 78}         -- "Temple Baa"
end

event 100
      Hint = str[1]  -- "Drink from Fountain"
  0:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  1:  StatusText  {Str = 3}         -- "Refreshing!"
  2:  GoTo  {jump = 6}

  3:  Subtract  {"MapVar0", Value = 1}
  4:  Add  {"SP", Value = 100}
  5:  Add  {"HP", Value = 100}
  6:  StatusText  {Str = 2}         -- "+100 Power temporary."
  7:  Set  {"AutonotesBits", Value = 52}         -- "100 Hit points and spell points from fountain in the desert in Paradise Valley."
  8:  Exit  {}

  9:  OnRefillTimer  {EachWeek = true}
  10: Set  {"MapVar0", Value = 10}
end

event 200
      Hint = str[7]  -- "Obelisk"
  0:  SetMessage  {Str = 6}         -- "The surface of the obelisk is blood warm to the touch.  A message swims into view as you remove your hand:                                                                                                                                                            nhrh_aherheatvdi"
  1:  SimpleMessage  {}
  2:  Set  {"QBits", Value = 361}         -- NPC
  3:  Set  {"AutonotesBits", Value = 80}         -- "Obelisk Message # 2:  nhrh_aherheatvdi"
end
