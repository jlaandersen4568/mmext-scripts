str[0] = " "
str[1] = "Chest"
str[2] = "Exit"
str[3] = "Exit"
str[4] = "Exit"
str[5] = ""


event 1
  0:  SetDoorState  {Id = 1, State = 0}
  1:  SetDoorState  {Id = 3, State = 1}
end

event 2
  0:  SetDoorState  {Id = 2, State = 0}
  1:  SetDoorState  {Id = 4, State = 1}
end

event 3
  0:  SetDoorState  {Id = 1, State = 1}
  1:  SetDoorState  {Id = 3, State = 0}
end

event 5
      Hint = str[1]  -- "Chest"
  0:  OpenChest  {Id = 2}
end

event 6
      Hint = str[1]  -- "Chest"
  0:  OpenChest  {Id = 3}
end
