str[0] = " "
str[1] = "Crumbling boulder."
str[2] = "The vent brings in fresh air."
str[3] = "Poisonous spores force you back."
str[4] = "Vent."
str[5] = "The air smells stale and makes you cough."
str[6] = "A small nymph comes out of a hole in the floor thanking you for clearing the spores, he shakes your hand vigorously then disappears down the hole (which somehow disappears after him). +500 experience."
str[7] = "Small switch."
str[8] = ""
str[9] = "Chest"
str[10] = ""


event 1
  0:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  1:  StatusText  {Str = 5}         -- "The air smells stale and makes you cough."
  2:  Add  {"MapVar0", Value = 1}
  3:  Exit  {}
end

event 2
  0:  Cmp  {"MapVar1", Value = 1,   jump = 5}
  1:  StatusText  {Str = 3}         -- "Poisonous spores force you back."
  2:  DamagePlayer  {Player = "All", DamageType = const.Damage.Cold, Damage = 8}
  3:  MoveToMap  {X = -128, Y = -1152, Z = 0, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  4:  Set  {"MapVar2", Value = 1}
  5:  Exit  {}
end

event 3
      Hint = str[1]  -- "Crumbling boulder."
  0:  Cmp  {"MapVar2", Value = 1,   jump = 2}
  1:  Exit  {}

  2:  StatusText  {Str = 2}         -- "The vent brings in fresh air."
  3:  SetSprite  {SpriteId = 1, Visible = 0, Name = "0"}
  4:  SetSprite  {SpriteId = 2, Visible = 0, Name = "0"}
  5:  Set  {"MapVar1", Value = 1}
end

event 4
      Hint = str[7]  -- "Small switch."
  0:  SetTexture  {Facet = 99, Name = "D8s2on"}
  1:  SetDoorState  {Id = 1, State = 1}
end

event 5
      Hint = str[9]  -- "Chest"
  0:  OpenChest  {Id = 0}
end

event 6
  0:  Cmp  {"MapVar3", Value = 1,   jump = 2}
  1:  Cmp  {"MapVar1", Value = 1,   jump = 3}
  2:  Exit  {}

  3:  SetMessage  {Str = 6}         -- "A small nymph comes out of a hole in the floor thanking you for clearing the spores, he shakes your hand vigorously then disappears down the hole (which somehow disappears after him). +500 experience."
  4:  SimpleMessage  {}
  5:  ForPlayer  ("All")
  6:  Add  {"Experience", Value = 500}
  7:  Add  {"MapVar3", Value = 1}
end
