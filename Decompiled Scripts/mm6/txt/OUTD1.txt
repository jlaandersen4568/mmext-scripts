str[0] = " "
str[1] = "Circle of Stones"
str[2] = "Chest"
str[3] = "The Sword won't budge!"
str[4] = "Drink from Well."
str[5] = "+20 Intellect and Personality temporary."
str[6] = "Drink from Fountain"
str[7] = "+25 Spell points restored."
str[8] = "WOW!"
str[9] = "+2 Accuracy permanent."
str[10] = "+2 Speed permanent."
str[11] = "Refreshing!"
str[12] = "Silver Cove"
str[13] = "Shrine of Personality"
str[14] = "You pray at the shrine."
str[15] = "+10 Personality permanent"
str[16] = "+3 Personality permanent"
str[17] = "The surface of the obelisk is blood warm to the touch.  A message swims into view as you remove your hand:                                                                                                                                                            nnaifnt_ieif_tu_"
str[18] = "Obelisk"
str[19] = "Castle Stone"
str[20] = "Silver Cove"
str[21] = ""
str[22] = ""
str[23] = ""
str[24] = ""
str[25] = ""
str[26] = ""
str[27] = ""
str[28] = ""
str[29] = ""


event 2
      Hint = str[10]  -- "+2 Speed permanent."
      MazeInfo = str[12]  -- "Silver Cove"
  0:  EnterHouse  {Id = 10}         -- "Abdul's Discount Weapons"
end

event 3
      Hint = str[10]  -- "+2 Speed permanent."
  0:  Exit  {}

  1:  EnterHouse  {Id = 10}         -- "Abdul's Discount Weapons"
end

event 4
      Hint = str[22]  -- ""
  0:  EnterHouse  {Id = 22}         -- "Abdul's Discount Armor"
end

event 5
      Hint = str[22]  -- ""
  0:  Exit  {}

  1:  EnterHouse  {Id = 22}         -- "Abdul's Discount Armor"
end

event 6
      Hint = str[32]
  0:  EnterHouse  {Id = 32}         -- "Abdul's Discount Magic Supplies"
end

event 7
      Hint = str[32]
  0:  Exit  {}

  1:  EnterHouse  {Id = 32}         -- "Abdul's Discount Magic Supplies"
end

event 8
      Hint = str[44]
  0:  EnterHouse  {Id = 44}         -- "Trader Joe's"
end

event 9
      Hint = str[44]
  0:  Exit  {}

  1:  EnterHouse  {Id = 44}         -- "Trader Joe's"
end

event 10
      Hint = str[53]
  0:  EnterHouse  {Id = 53}         -- "Abdul's Discount Travel"
end

event 11
      Hint = str[53]
  0:  Exit  {}

  1:  EnterHouse  {Id = 53}         -- "Abdul's Discount Travel"
end

event 12
      Hint = str[61]
  0:  EnterHouse  {Id = 61}         -- "Cerulean Skies"
end

event 13
      Hint = str[75]
  0:  EnterHouse  {Id = 75}         -- "Silver Cove Temple"
end

event 14
      Hint = str[81]
  0:  EnterHouse  {Id = 81}         -- "Abdul's Discount Training Center"
end

event 15
      Hint = str[81]
  0:  Exit  {}

  1:  EnterHouse  {Id = 81}         -- "Abdul's Discount Training Center"
end

event 16
      Hint = str[91]
  0:  EnterHouse  {Id = 91}         -- "Town Hall"
end

event 17
      Hint = str[100]
  0:  EnterHouse  {Id = 100}         -- "Anchors Away"
end

event 18
      Hint = str[100]
  0:  Exit  {}

  1:  EnterHouse  {Id = 100}         -- "Anchors Away"
end

event 19
      Hint = str[101]
  0:  EnterHouse  {Id = 101}         -- "The Grove"
end

event 20
      Hint = str[101]
  0:  Exit  {}

  1:  EnterHouse  {Id = 101}         -- "The Grove"
end

event 21
      Hint = str[115]
  0:  EnterHouse  {Id = 115}         -- "The First Bank of Enroth"
end

event 22
      Hint = str[115]
  0:  Exit  {}

  1:  EnterHouse  {Id = 115}         -- "The First Bank of Enroth"
end

event 23
      Hint = str[125]
  0:  EnterHouse  {Id = 125}         -- "Initiate Guild of Earth"
end

event 24
      Hint = str[125]
  0:  Exit  {}

  1:  EnterHouse  {Id = 125}         -- "Initiate Guild of Earth"
end

event 25
      Hint = str[133]
  0:  EnterHouse  {Id = 133}         -- "Initiate Guild of Light"
end

event 26
      Hint = str[133]
  0:  Exit  {}

  1:  EnterHouse  {Id = 133}         -- "Initiate Guild of Light"
end

event 27
      Hint = str[140]
  0:  EnterHouse  {Id = 140}         -- "Adept Guild of the Self"
end

event 28
      Hint = str[140]
  0:  Exit  {}
end

event 29
      Hint = str[143]
  0:  EnterHouse  {Id = 143}         -- "Berserkers' Fury"
end

event 30
      Hint = str[143]
  0:  Exit  {}

  1:  EnterHouse  {Id = 143}         -- "Berserkers' Fury"
end

event 31
      Hint = str[150]
  0:  EnterHouse  {Id = 150}         -- "Protection Services"
end

event 32
      Hint = str[63]
  0:  EnterHouse  {Id = 63}         -- "Barracuda"
end

event 33
      Hint = str[166]
  0:  EnterHouse  {Id = 166}         -- "Circus"
end

event 34
      Hint = str[161]
  0:  MoveToMap  {X = 0, Y = 0, Z = 0, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 161, Icon = 2, Name = "0"}         -- "Castle Fleise"
  1:  EnterHouse  {Id = 162}         -- "Throne Room"
end

event 35
      Hint = str[19]  -- "Castle Stone"
  0:  StatusText  {Str = 19}         -- "Castle Stone"
end

event 36
      Hint = str[20]  -- "Silver Cove"
  0:  StatusText  {Str = 20}         -- "Silver Cove"
end

event 50
      Hint = str[110]
  0:  EnterHouse  {Id = 366}         -- "House"
end

event 51
      Hint = str[111]
  0:  EnterHouse  {Id = 367}         -- "House"
end

event 52
      Hint = str[112]
  0:  EnterHouse  {Id = 368}         -- "House"
end

event 53
      Hint = str[113]
  0:  EnterHouse  {Id = 369}         -- "House"
end

event 54
      Hint = str[114]
  0:  EnterHouse  {Id = 370}         -- "House"
end

event 55
      Hint = str[115]
  0:  EnterHouse  {Id = 371}         -- "House"
end

event 56
      Hint = str[116]
  0:  EnterHouse  {Id = 372}         -- "House"
end

event 57
      Hint = str[117]
  0:  EnterHouse  {Id = 373}         -- "House"
end

event 58
      Hint = str[118]
  0:  EnterHouse  {Id = 374}         -- "House"
end

event 59
      Hint = str[119]
  0:  EnterHouse  {Id = 375}         -- "House"
end

event 60
      Hint = str[120]
  0:  EnterHouse  {Id = 376}         -- "House"
end

event 61
      Hint = str[121]
  0:  EnterHouse  {Id = 377}         -- "House"
end

event 62
      Hint = str[122]
  0:  EnterHouse  {Id = 378}         -- "House"
end

event 63
      Hint = str[123]
  0:  EnterHouse  {Id = 379}         -- "House"
end

event 64
      Hint = str[124]
  0:  EnterHouse  {Id = 380}         -- "House"
end

event 65
      Hint = str[125]
  0:  EnterHouse  {Id = 381}         -- "House"
end

event 66
      Hint = str[126]
  0:  EnterHouse  {Id = 382}         -- "House"
end

event 67
      Hint = str[127]
  0:  EnterHouse  {Id = 383}         -- "House"
end

event 68
      Hint = str[128]
  0:  EnterHouse  {Id = 384}         -- "House"
end

event 69
      Hint = str[129]
  0:  EnterHouse  {Id = 385}         -- "House"
end

event 70
      Hint = str[130]
  0:  EnterHouse  {Id = 386}         -- "House"
end

event 71
      Hint = str[131]
  0:  EnterHouse  {Id = 387}         -- "House"
end

event 72
      Hint = str[132]
  0:  EnterHouse  {Id = 388}         -- "House"
end

event 73
      Hint = str[133]
  0:  EnterHouse  {Id = 389}         -- "House"
end

event 74
      Hint = str[134]
  0:  EnterHouse  {Id = 390}         -- "House"
end

event 75
      Hint = str[135]
  0:  EnterHouse  {Id = 391}         -- "House"
end

event 76
      Hint = str[136]
  0:  EnterHouse  {Id = 392}         -- "House"
end

event 77
      Hint = str[137]
  0:  EnterHouse  {Id = 393}         -- "House"
end

event 78
      Hint = str[138]
  0:  EnterHouse  {Id = 394}         -- "House"
end

event 79
      Hint = str[139]
  0:  EnterHouse  {Id = 395}         -- "House"
end

event 80
      Hint = str[140]
  0:  EnterHouse  {Id = 396}         -- "House"
end

event 81
      Hint = str[141]
  0:  EnterHouse  {Id = 397}         -- "House"
end

event 82
      Hint = str[142]
  0:  EnterHouse  {Id = 398}         -- "House"
end

event 83
      Hint = str[143]
  0:  EnterHouse  {Id = 399}         -- "House"
end

event 84
      Hint = str[144]
  0:  EnterHouse  {Id = 400}         -- "House"
end

event 85
      Hint = str[145]
  0:  EnterHouse  {Id = 401}         -- "House"
end

event 86
      Hint = str[146]
  0:  EnterHouse  {Id = 402}         -- "House"
end

event 87
      Hint = str[147]
  0:  EnterHouse  {Id = 403}         -- "House"
end

event 88
      Hint = str[148]
  0:  EnterHouse  {Id = 404}         -- "House"
end

event 89
      Hint = str[149]
  0:  EnterHouse  {Id = 405}         -- "House"
end

event 90
      Hint = str[150]
  0:  EnterHouse  {Id = 406}         -- "House"
end

event 91
      Hint = str[151]
  0:  EnterHouse  {Id = 407}         -- "House"
end

event 92
      Hint = str[152]
  0:  EnterHouse  {Id = 408}         -- "House"
end

event 93
      Hint = str[153]
  0:  EnterHouse  {Id = 409}         -- "House"
end

event 94
      Hint = str[154]
  0:  EnterHouse  {Id = 410}         -- "House"
end

event 95
      Hint = str[155]
  0:  EnterHouse  {Id = 411}         -- "House"
end

event 100
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 1}
end

event 101
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 2}
end

event 102
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 3}
end

event 150
  0:  MoveToMap  {X = -127, Y = 4190, Z = 1, Direction = 1536, LookAngle = 0, SpeedZ = 0, HouseId = 182, Icon = 5, Name = "D12.Blv"}         -- "Silver Helm Stronghold"
  1:  Exit  {}

      Hint = str[0]  -- " "
  2:  EnterHouse  {Id = 182}         -- "Silver Helm Stronghold"
end

event 151
  0:  MoveToMap  {X = -128, Y = -3968, Z = 1, Direction = 512, LookAngle = 0, SpeedZ = 0, HouseId = 183, Icon = 5, Name = "D13.Blv"}         -- "The Monolith"
  1:  Exit  {}

      Hint = str[0]  -- " "
  2:  EnterHouse  {Id = 183}         -- "The Monolith"
end

event 152
  0:  MoveToMap  {X = -4724, Y = 1494, Z = 127, Direction = 1920, LookAngle = 0, SpeedZ = 0, HouseId = 186, Icon = 5, Name = "D16.Blv"}         -- "Warlord's Fortress"
  1:  Exit  {}

      Hint = str[0]  -- " "
  2:  EnterHouse  {Id = 186}         -- "Warlord's Fortress"
end

event 161
      Hint = str[4]  -- "Drink from Well."
  0:  Cmp  {"IntellectBonus", Value = 20,   jump = 6}
  1:  Set  {"IntellectBonus", Value = 20}
  2:  Set  {"PersonalityBonus", Value = 20}
  3:  StatusText  {Str = 5}         -- "+20 Intellect and Personality temporary."
  4:  Set  {"AutonotesBits", Value = 25}         -- "20 Points of temporary intellect and personality from the fountain in the north side of Silver Cove."
  5:  Exit  {}

  6:  StatusText  {Str = 11}         -- "Refreshing!"
  7:  Exit  {}
end

event 162
      Hint = str[6]  -- "Drink from Fountain"
  0:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  1:  StatusText  {Str = 11}         -- "Refreshing!"
  2:  GoTo  {jump = 6}

  3:  Subtract  {"MapVar0", Value = 1}
  4:  Add  {"SP", Value = 25}
  5:  StatusText  {Str = 7}         -- "+25 Spell points restored."
  6:  Set  {"AutonotesBits", Value = 26}         -- "25 Spell points restored by the fountain outside of Town Hall in Silver Cove."
  7:  Exit  {}

  8:  OnRefillTimer  {Second = 1}
  9:  Set  {"MapVar0", Value = 20}
end

event 163
      Hint = str[6]  -- "Drink from Fountain"
  0:  Set  {"Insane", Value = 0}
  1:  StatusText  {Str = 8}         -- "WOW!"
end

event 164
      Hint = str[6]  -- "Drink from Fountain"
  0:  Cmp  {"BaseAccuracy", Value = 15,   jump = 2}
  1:  Cmp  {"MapVar1", Value = 1,   jump = 4}
  2:  StatusText  {Str = 11}         -- "Refreshing!"
  3:  Exit  {}

  4:  Subtract  {"MapVar1", Value = 1}
  5:  Add  {"BaseAccuracy", Value = 2}
  6:  StatusText  {Str = 9}         -- "+2 Accuracy permanent."
  7:  Set  {"AutonotesBits", Value = 27}         -- "2 Points of permanent accuracy from the north fountain west of Silver Cove."
  8:  Exit  {}

  9:  OnRefillTimer  {EachMonth = true}
  10: Set  {"MapVar1", Value = 8}
end

event 165
      Hint = str[6]  -- "Drink from Fountain"
  0:  Cmp  {"BaseSpeed", Value = 15,   jump = 2}
  1:  Cmp  {"MapVar2", Value = 1,   jump = 4}
  2:  StatusText  {Str = 11}         -- "Refreshing!"
  3:  Exit  {}

  4:  Subtract  {"MapVar2", Value = 1}
  5:  Add  {"BaseSpeed", Value = 2}
  6:  StatusText  {Str = 10}         -- "+2 Speed permanent."
  7:  Set  {"AutonotesBits", Value = 28}         -- "2 Points of permanent speed from the south fountain west of Silver Cove."
  8:  Exit  {}

  9:  OnRefillTimer  {EachMonth = true}
  10: Set  {"MapVar2", Value = 8}
end

event 209
  0:  OnTimer  {IntervalInHalfMinutes = 10}
  1:  Cmp  {"QBits", Value = 158,   jump = 3}         -- NPC
  2:  Cmp  {"Flying", Value = 0,   jump = 4}
  3:  Exit  {}

  4:  CastSpell  {Spell = 6, Mastery = const.Master, Skill = 5, FromX = 11032, FromY = -8940, FromZ = 2830, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
end

event 210
  0:  ForPlayer  ("All")
  1:  Cmp  {"QBits", Value = 158,   jump = 3}         -- NPC
  2:  Cmp  {"Inventory", Value = 486,   jump = 4}         -- "Dragon Tower Keys"
  3:  Exit  {}

  4:  Set  {"QBits", Value = 158}         -- NPC
  5:  SetTextureOutdoors  {Model = 117, Facet = 42, Name = "T1swBu"}
end

event 211
      Hint = str[1]  -- "Circle of Stones"
  0:  Cmp  {"QBits", Value = 173,   jump = 5}         -- NPC
  1:  Cmp  {"DayOfYearIs", Value = 76,   jump = 8}
  2:  Cmp  {"DayOfYearIs", Value = 161,   jump = 8}
  3:  Cmp  {"DayOfYearIs", Value = 247,   jump = 8}
  4:  Cmp  {"DayOfYearIs", Value = 329,   jump = 8}
  5:  Exit  {}

  6:  SpeakNPC  {NPC = 305}         -- "Loretta Fleise"
  7:  Exit  {}

  8:  Cmp  {"QBits", Value = 118,   jump = 6}         -- "Visit the Altar of the Sun in the circle of stones north of Silver Cove on an equinox or solstice (HINT:  March 20th is an equinox)."
end

event 212
  0:  MoveToMap  {X = -12344, Y = 17112, Z = 1, Direction = 1536, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 213
  0:  MoveToMap  {X = -9400, Y = 17184, Z = 1, Direction = 1536, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 214
  0:  MoveToMap  {X = -11512, Y = 19368, Z = 1, Direction = 1536, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 215
  0:  MoveToMap  {X = -9192, Y = 21936, Z = 160, Direction = 1536, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 220
  0:  Cmp  {"QBits", Value = 310,   jump = 8}         -- NPC
  1:  Cmp  {"CurrentMight", Value = 60,   jump = 5}
  2:  FaceExpression  {Player = "Current", Frame = 51}
  3:  StatusText  {Str = 3}         -- "The Sword won't budge!"
  4:  GoTo  {jump = 8}

  5:  Set  {"QBits", Value = 310}         -- NPC
  6:  GiveItem  {Strength = 5, Type = const.ItemType.Sword, Id = 4}         -- "Champion Sword"
  7:  SetSprite  {SpriteId = 359, Visible = 1, Name = "swrdstx"}
  8:  Exit  {}

  9:  OnLoadMap  {}
  10: Cmp  {"QBits", Value = 310,   jump = 7}         -- NPC
end

event 221
  0:  Cmp  {"QBits", Value = 311,   jump = 8}         -- NPC
  1:  Cmp  {"CurrentMight", Value = 60,   jump = 5}
  2:  FaceExpression  {Player = "Current", Frame = 51}
  3:  StatusText  {Str = 3}         -- "The Sword won't budge!"
  4:  GoTo  {jump = 8}

  5:  Set  {"QBits", Value = 311}         -- NPC
  6:  GiveItem  {Strength = 5, Type = const.ItemType.Sword, Id = 5}         -- "Lionheart Sword"
  7:  SetSprite  {SpriteId = 360, Visible = 1, Name = "swrdstx"}
  8:  Exit  {}

  9:  OnLoadMap  {}
  10: Cmp  {"QBits", Value = 311,   jump = 7}         -- NPC
end

event 222
  0:  Cmp  {"QBits", Value = 312,   jump = 8}         -- NPC
  1:  Cmp  {"CurrentMight", Value = 100,   jump = 5}
  2:  FaceExpression  {Player = "Current", Frame = 51}
  3:  StatusText  {Str = 3}         -- "The Sword won't budge!"
  4:  GoTo  {jump = 8}

  5:  Set  {"QBits", Value = 312}         -- NPC
  6:  GiveItem  {Strength = 6, Type = const.ItemType.Sword, Id = 8}         -- "Heroic Sword"
  7:  SetSprite  {SpriteId = 361, Visible = 1, Name = "swrdstx"}
  8:  Exit  {}

  9:  OnLoadMap  {}
  10: Cmp  {"QBits", Value = 312,   jump = 7}         -- NPC
end

event 223
  0:  OnLoadMap  {}
  1:  Cmp  {"QBits", Value = 179,   jump = 3}         -- NPC
  2:  Set  {"QBits", Value = 179}         -- NPC
  3:  Exit  {}
end

event 224
      Hint = str[18]  -- "Obelisk"
  0:  SetMessage  {Str = 17}         -- "The surface of the obelisk is blood warm to the touch.  A message swims into view as you remove your hand:                                                                                                                                                            nnaifnt_ieif_tu_"
  1:  SimpleMessage  {}
  2:  Set  {"QBits", Value = 369}         -- NPC
  3:  Set  {"AutonotesBits", Value = 88}         -- "Obelisk Message # 10: nnaifnt_ieif_tu_"
end

event 226
  0:  OnLoadMap  {}
  1:  Cmp  {"QBits", Value = 158,   jump = 3}         -- NPC
  2:  Exit  {}

  3:  SetTextureOutdoors  {Model = 117, Facet = 42, Name = "T1swBu"}
end

event 261
      Hint = str[13]  -- "Shrine of Personality"
  0:  Cmp  {"MonthIs", Value = 2,   jump = 3}
  1:  StatusText  {Str = 14}         -- "You pray at the shrine."
  2:  Exit  {}

  3:  Cmp  {"QBits", Value = 206,   jump = 1}         -- NPC
  4:  Set  {"QBits", Value = 206}         -- NPC
  5:  Cmp  {"QBits", Value = 209,   jump = 11}         -- NPC
  6:  Set  {"QBits", Value = 209}         -- NPC
  7:  ForPlayer  ("All")
  8:  Add  {"BasePersonality", Value = 10}
  9:  StatusText  {Str = 15}         -- "+10 Personality permanent"
  10: Exit  {}

  11: ForPlayer  ("All")
  12: Add  {"BasePersonality", Value = 3}
  13: StatusText  {Str = 16}         -- "+3 Personality permanent"
end
