str[0] = " "
str[1] = ""
str[2] = ""
str[3] = ""
str[4] = ""
str[5] = "Stone of Revelation"
str[6] = "You cannot read the inscription."
str[7] = "Revelation Msg#3.  Once inside, follow the Path of Lights."
str[8] = "Autonote added."
str[9] = ""
str[10] = ""
str[11] = ""
str[12] = ""
str[13] = "Well, thanks for sneaking me out of the Castle.  Sorry about the circus thing�I hope I wasn�t too much trouble to find.  I�ll go in myself so no one will see that it was you who kidnapped me.  Thanks again, and goodbye.  I�ll remember this, and I owe you a favor! "
str[14] = ""
str[15] = ""
str[16] = ""
str[17] = ""
str[18] = ""
str[19] = ""
str[20] = ""
str[21] = ""
str[22] = ""
str[23] = ""
str[24] = ""
str[25] = ""
str[26] = ""
str[27] = ""
str[28] = ""
str[29] = ""
str[30] = ""
str[31] = ""
str[32] = ""
str[33] = ""
str[34] = ""
str[35] = ""
str[36] = ""
str[37] = ""


event 1
  0:  OnLoadMap  {}
  1:  Cmp  {"QBits", Value = 511,   jump = 5}         -- SOG Void Teleport
  2:  ForPlayer  ("All")
  3:  Set  {"Eradicated", Value = 0}
  4:  Exit  {}

  5:  Cmp  {"QBits", Value = 330,   jump = 7}         -- NPC
  6:  GoTo  {jump = 2}

  7:  Exit  {}
end

event 10
      Hint = str[5]  -- "Stone of Revelation"
  0:  Cmp  {"QBits", Value = 505,   jump = 3}         -- Stone of Translation
  1:  StatusText  {Str = 6}         -- "You cannot read the inscription."
  2:  Exit  {}

  3:  Add  {"QBits", Value = 494}         -- Tamara Once
  4:  SetMessage  {Str = 7}         -- "Revelation Msg#3.  Once inside, follow the Path of Lights."
  5:  SimpleMessage  {}
  6:  Add  {"AutonotesBits", Value = 81}         -- "Revelation Msg#3.  Once inside, follow the Path of Lights."
  7:  StatusText  {Str = 8}         -- "Autonote added."
  8:  Exit  {}
end
